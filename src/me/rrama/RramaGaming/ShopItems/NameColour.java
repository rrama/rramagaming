package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.TitleCMDs;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class NameColour extends ShopItem {
    
    public NameColour(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (TitleCMDs.HasANameColourToUse.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You already have a name colour to use. Use /NameColour [Colour].");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        TitleCMDs.HasANameColourToUse.add(P.getName());
        return true;
    }
    
}