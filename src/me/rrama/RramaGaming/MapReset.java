package me.rrama.RramaGaming;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import java.io.File;
import java.io.IOException;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MapReset {
    
    public static void ResetAllMaps() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.kickPlayer("All the maps are resetting relog in soon.");
        }
        for (GamingMap GM : GamingMaps.GamingMaps.values()) {
            ResetMap(GM);
        }
    }
    
    public static void ResetMap(final String MapName) {
        ResetMap(GamingMaps.GamingMaps.get(MapName));
    }
    
    public static void ResetMap(final GamingMap GM) {
        String MapName = GM.getName();
        File file = new File((FolderSetup.SchematicsFolder + System.getProperty("file.separator") + MapName + ".schematic"));
        if (file.exists()) {
            try {
                Vector v = GM.getResetPoint();
                World worldf = Bukkit.getWorlds().get(0);
                BukkitWorld BWf = new BukkitWorld(worldf);
                EditSession es = new EditSession(BWf, 2147483647);
                CuboidClipboard c1 = SchematicFormat.MCEDIT.load(file);
                OpChat.SendMessageToAllOps("RramaGaming", MapName + " is reseting now.");
                c1.place(es, v, false);
                RGAchievements.AwardAchievement(Bukkit.getOnlinePlayers(), Achievement.InB4Kippers);
            } catch (DataException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "'ResetMap()' DataException"));
            } catch (IOException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "'ResetMap()' IOException"));
            } catch (MaxChangedBlocksException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "'ResetMap()' MaxChangedBlocksException"));
            }
        } else {
            Bukkit.getLogger().warning((RramaGaming.RGTagB + "'" + MapName + ".schematic' Does not exsist in '" + FolderSetup.SchematicsFolder + "'!"));
            OpChat.SendMessageToAllOps("RramaGaming", "Cannot reset the map. File not found.");
        }
    }
    
    public static class ResetMapCMD implements CommandExecutor {
        
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("ResetMap") && args.length == 1) {
                String s = args[0];
                if (s.equalsIgnoreCase("all")) {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " is resetting all the maps.");
                    ResetAllMaps();
                    return true;
                } else if (GamingMaps.GamingMaps.keySet().contains(s)) {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " is resetting " + s + ".");
                    ResetMap(s);
                    return true;
                } else return false;
            } else return false;
        }
    }
}