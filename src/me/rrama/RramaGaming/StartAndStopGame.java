package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.plugin.Plugin;

public class StartAndStopGame implements TabExecutor {
    
    public static boolean Cycle = false, Need2Cycle = false;
    
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLable, String[] args) {
        ArrayList<String> outcomes = new ArrayList<>();
        if (args.length != 1) {
            sender.sendMessage(ChatColor.YELLOW + "There is no command with arguements after what you have. Retype the command.");
            return outcomes;
        }
        if (commandLable.equalsIgnoreCase("GameStart")) {
            if (!RramaGaming.gameInPlay.equals("null") && RramaGaming.R == RoundState.PreGame) {
                GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                if (GP.ForceStart) {
                    outcomes.add("Force");
                }
            }
            outcomes.add("Cycle");
            outcomes.addAll(RramaGaming.EnabledGames);
        } else if (commandLable.equalsIgnoreCase("UnloadGame")) {
            outcomes.addAll(RramaGaming.EnabledGames);
        } else if (commandLable.equalsIgnoreCase("LoadGame")) {
            outcomes.addAll(RramaGaming.DisabledGames);
        } else outcomes.add("There is no command with arguements after what you have. Retype the command.");
        
        if (outcomes.isEmpty()) {
            sender.sendMessage(ChatColor.YELLOW + "No games must be loaded/unloaded (depends on command trying used).");
        }
        return outcomes;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        
        if (commandLable.equalsIgnoreCase("LoadedGames")) {
            sender.sendMessage(ChatColor.YELLOW + "Loaded games: " + ListToString.ListToString(RramaGaming.EnabledGames, ", "));
            return true;
            
        } else if (commandLable.equalsIgnoreCase("UnloadedGames")) {
            sender.sendMessage(ChatColor.YELLOW + "Unloaded games: " + ListToString.ListToString(RramaGaming.DisabledGames, ", "));
            return true;
            
        } else if (commandLable.equalsIgnoreCase("GameStart")) {
            if (args.length != 1) return false;
            if (args[0].equalsIgnoreCase("Force")) {
                if (RramaGaming.gameInPlay.equals("null")) {
                    sender.sendMessage(ChatColor.YELLOW + "No game in play.");
                } else {
                    GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                    String GPN = GP.getName();
                    if (!GP.ForceStart) {
                        sender.sendMessage(ChatColor.YELLOW + "Can not force start " + GPN + ".");
                    } else if (RramaGaming.R == RoundState.PreGame) {
                        GP.timer.cancel();
                        GP.callStartRound();
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "Can not force a start at this time.");
                    }
                }
                return true;
            
            } else if (args[0].equalsIgnoreCase("Cycle")) {
                if (Cycle) {
                    Cycle = false;
                    OpChat.SendMessageToAllOps("RramaGaming", "GameCycle disabled by " + sender.getName() + ".");
                } else {
                    OpChat.SendMessageToAllOps("RramaGaming", "GameCycle enabled by " + sender.getName() + ".");
                    if (!RramaGaming.gameInPlay.equals("null")) {
                        Cycle = true;
                    } else {
                        CycleGame();
                    }
                }
                return true;
                
            } else {
                if (!RramaGaming.EnabledGames.contains(args[0])) {
                    sender.sendMessage(ChatColor.YELLOW + "No loaded game by that name. Check spelling, capitals and if it's loaded.");
                    return true;
                }
                GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(args[0]);
                String GPN = GP.getName();
                if (!GP.RoundPlugin) {
                    sender.sendMessage(ChatColor.YELLOW + "This game does not have rounds.");
                } else if (!RramaGaming.gameInPlay.equals(GPN) || RramaGaming.R == RoundState.Off) {
                    if (RramaGaming.gameInPlay.equals("null")) {
                        for (Gamer G : RramaGaming.getGamers()) {
                            if (G.getGamePlaying().equals("null")) G.setGamePlaying(GPN);
                        }
                        RramaGaming.gameInPlay = GPN;
                        OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " started " + GPN + ".");
                        MapVote(GP);
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + RramaGaming.gameInPlay + " is already in play. Stop " + RramaGaming.gameInPlay + " first.");
                    }
                } else if (RramaGaming.R == RoundState.InGame) {
                    sender.sendMessage(ChatColor.YELLOW + "Round has already started.");
                } else if (RramaGaming.R == RoundState.AfterGame) {
                    sender.sendMessage(ChatColor.YELLOW + "Can not start " + GPN + " whilst Cookies are being given out.");
                } else if (RramaGaming.R == RoundState.Voting) {
                    sender.sendMessage(ChatColor.YELLOW + "Can not start " + GPN + " in a Map Vote.");
                } else if (RramaGaming.R == RoundState.MapReset) {
                    sender.sendMessage(ChatColor.YELLOW + "Next map is resetting can not start or force yet.");
                }
                return true;
            }
            
        } else if (commandLable.equalsIgnoreCase("UnloadGame") && args.length == 1) {
            if (!RramaGaming.EnabledGames.contains(args[0])) {
                sender.sendMessage(ChatColor.YELLOW + "No loaded game by that name. Check spelling, capitals and if it's loaded.");
                return true;
            }
            GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(args[0]);
            String GPN = GP.getName();
            if (RramaGaming.gameInPlay.equals(GPN)) {
                sender.sendMessage(ChatColor.YELLOW + "Can not unload " + GPN + " whilst it is in play. Stop it first.");
                return true;
            }
            OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " is unloading " + GPN + ".");
            Bukkit.getPluginManager().disablePlugin(GP);
            OpChat.SendMessageToAllOps("RramaGaming", GP.getName() + " unloaded.");
            return true;
            
        } else if (commandLable.equalsIgnoreCase("LoadGame") && args.length == 1) {
            Plugin P = Bukkit.getPluginManager().getPlugin(args[0]);
            if (P == null) {
                sender.sendMessage(ChatColor.YELLOW + "Can not find that plugin. Check the /UnloadedGames and capitals.");
                return true;
            }
            if (P.isEnabled()) {
                sender.sendMessage(ChatColor.YELLOW + P.getName() + " is already enabled.");
                return true;
            }
            OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " is loading " + P.getName() + ".");
            Bukkit.getPluginManager().enablePlugin(P);
            OpChat.SendMessageToAllOps("RramaGaming", P.getName() + " loaded.");
            return true;
            
        } else if (!RramaGaming.gameInPlay.equals("null")) {
            GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
            String GPN = GP.getName();
            
            if (commandLable.equalsIgnoreCase("GameStop")) {
                if (RramaGaming.R == RoundState.InGame) {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " stopped " + GPN + ".");
                    GP.callEndRound(true);
                } else if (RramaGaming.R == RoundState.PreGame) {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " stopped " + GPN + ".");
                    GP.timer.cancel();
                    RramaGaming.gameInPlay = "null";
                    for (Gamer G : RramaGaming.getGamers()) {
                        if (G.m()) {
                            G.setGamePlaying("null");
                            G.setTagColour(ChatColor.WHITE);
                        }
                    }
                    RramaGaming.R = RoundState.Off;
                    RramaGaming.WasInRound.clear();
                    RramaGaming.teleportAllPlayersTo(GamingMaps.GamingMaps.get("Spawn").getSpawn());
                } else if (RramaGaming.R == RoundState.Voting || RramaGaming.R == RoundState.AfterGame || RramaGaming.R == RoundState.MapReset) {
                    sender.sendMessage(ChatColor.YELLOW + "Can not stop " + GPN + " at this time.");
                } else if (RramaGaming.R == RoundState.Off) {
                    sender.sendMessage(ChatColor.YELLOW + GPN + " is in play, but round is off... BREAK BREAK!");
                } else {
                    sender.sendMessage(ChatColor.YELLOW + GPN + "is in play, but in an unexpected round state '" + RramaGaming.R.name() + "'.");
                }
                return true;

            } else if (commandLable.equalsIgnoreCase("EndRound")) {
                if (RramaGaming.R == RoundState.InGame) {
                    OpChat.SendMessageToAllOps(GPN, sender.getName() + " ended the round.");
                    GP.callEndRound(false);
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Round is not in play.");
                }
                return true;
            } else return false;
        } else {
            sender.sendMessage(ChatColor.YELLOW + "No game is currently in play.");
            return true;
        }
    }
    
    public static void MapVote(GamingPlugin GP) {
        if (Cycle && Need2Cycle && RramaGaming.R == RoundState.Off) {
            CycleGame();
        } else {
            Need2Cycle = true;
            if (RramaGaming.MoreAvalibleGamersThan(GP.MinimumPlayersNeeded)) {
                Vote.MapVote(GP);
            } else {
                RramaGaming.R = RoundState.NeedMorePlayers;
                Bukkit.broadcastMessage(GP.TagNo + ChatColor.RED + "Need at least "+GP.MinimumPlayersNeeded+" players to start "+GP.getName()+".");
                RramaGaming.MapInPlay = GamingMaps.GamingMaps.get("Spawn");
                RramaGaming.teleportAllPlayersTo(GamingMaps.GamingMaps.get("Spawn").getSpawn());
            }
        }
    }
    
    public static void CycleGame() {
        if (!RramaGaming.gameInPlay.equals("null")) {
            OpChat.SendMessageToAllOps("RramaGaming", "Could not cycle games as a game is in play.");
            return;
        }
        ArrayList<String> EnabledGamesClone = new ArrayList<>(), AvalibleGames = new ArrayList<>(); //Stop ConcurrentModification.
        EnabledGamesClone.addAll(RramaGaming.EnabledGames);
        AvalibleGames.addAll(RramaGaming.EnabledGames);
        AvalibleGames.addAll(RramaGaming.DisabledGames);
        
        if (AvalibleGames.isEmpty()) {
            OpChat.SendMessageToAllOps("RramaGaming", "Could not cycle games as there are no loaded or unloaded rounded games.");
            return;
        }
        
        GamingPlugin GP;
        String GPN;        
        //Get random game to play.
        do {
            Random R = new Random();
            GPN = AvalibleGames.get(R.nextInt(AvalibleGames.size()-1));
            GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(GPN);
            EnabledGamesClone.remove(GPN); //Don't diable random game.
        } while (!(GP.RoundPlugin && (RramaGaming.MoreAvalibleGamersThan(GP.MinimumPlayersNeeded) || GP.MinimumPlayersNeeded == 1)));
        
        for (String GPS : EnabledGamesClone) { //Disable other loaded games (better CPU/RAM).
            if (GP.RoundPlugin) {
                Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin(GPS));
            }
        }
        
        Bukkit.getPluginManager().enablePlugin(GP);
        
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.getGamePlaying().equals("null")) G.setGamePlaying(GPN);
        }
        RramaGaming.gameInPlay = GPN;
        OpChat.SendMessageToAllOps("RramaGaming", "GameCycle started " + GPN + ".");
        Cycle = true;
        Need2Cycle = false;
        MapVote(GP);
    }
    
}