package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class Vote {
    
    public static int map1Votes, map2Votes;
    public static GamingMap map1, map2;
    public static ArrayList<CommandSender> HasVoted = new ArrayList<>();
    protected static GamingPlugin GPD;
    protected static BukkitTask VoteTask;
    
    public static void MapVote(final GamingPlugin GP) {
        GPD = GP;
        RramaGaming.R = RoundState.Voting;
        map1Votes = 0;
        map2Votes = 0;
        HasVoted.clear();
        int map1int = RramaGaming.Random.nextInt(GP.Maps.size());
        int map2int = RramaGaming.Random.nextInt(GP.Maps.size());
        map1 = GamingMaps.GamingMaps.get(GP.Maps.get(map1int));
        map2 = GamingMaps.GamingMaps.get(GP.Maps.get(map2int));
        Bukkit.broadcastMessage(GP.TagB + "Map Vote:");
        Bukkit.broadcastMessage(ChatColor.BLUE + "Map 1: " + ChatColor.RED + map1.getName() + ChatColor.BLUE + " Map 2: " + ChatColor.GREEN + map2.getName());
        Bukkit.broadcastMessage(ChatColor.BLUE + "Vote with " + ChatColor.YELLOW + "/vote " + ChatColor.RED + "1 " + ChatColor.YELLOW + "or " + ChatColor.GREEN + "2" + ChatColor.BLUE + ".");

        VoteTask = Bukkit.getScheduler().runTaskLaterAsynchronously(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                Vote.GetResults();
            }
        }, 900);
    }
    
    public static void AddVote(String s) {
        switch (s) {
            case "1":
                map1Votes++;
                break;
            case "2":
                map2Votes++;
                break;
            default:
                Bukkit.getLogger().info(("A map vote has failed, at point: AddVote("+s+")"));
                break;
        }
    }
    
    public static void GetResults() {
        GamingMap WinningMap;
        Bukkit.broadcastMessage(GPD.TagB + "Voting ended.");
        RramaGaming.R = RoundState.MapReset;
        if ((map1Votes == 0) && (map2Votes == 0)) {
            Bukkit.broadcastMessage(GPD.TagB + "No votes were collected. Resorting to map: " + ChatColor.RED + map1.getName());
            WinningMap = map1;
        } else {
            Bukkit.broadcastMessage(GPD.TagNo + ChatColor.RED + map1.getName()  + ChatColor.BLUE + " got " + map1Votes + " votes. " + ChatColor.GREEN + map2.getName() + ChatColor.BLUE + " got " + map2Votes + " votes.");
            if (map1Votes == map2Votes) {
                Bukkit.broadcastMessage(GPD.TagB + "It's a draw! Resorting to map: " + ChatColor.RED + map1.getName());
                WinningMap = map1;
            } else if (map1Votes > map2Votes) {
                Bukkit.broadcastMessage(GPD.TagNo + ChatColor.RED + map1.getName() + ChatColor.BLUE + " won!");
                WinningMap = map1;
            } else {
                Bukkit.broadcastMessage(GPD.TagNo + ChatColor.GREEN + map2.getName() + ChatColor.BLUE + " won!");
                WinningMap = map2;
            }
        }
        for (Gamer G : RramaGaming.getGamers()) {
            G.sendTexturePack(GamingMaps.checkTextureURL(WinningMap.getTexture()));
        }
        MoveToNewMap(WinningMap);
    }
    
    public static void MoveToNewMap(final GamingMap WinningMap) {
        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() {
            
            @Override
            public void run() {
                for (Gamer G : RramaGaming.getGamersWhoCanPlay()) {
                    G.getPlayer().getInventory().clear();
                    G.removeAllPotions();
                    G.zeroExp();
                }
                Bukkit.broadcastMessage(GPD.TagB + "Next map is resetting. Inb4 kippers.");
                MapReset.ResetMap(WinningMap);
                MoveToNewMapP2(WinningMap);
            }
        }, 3*20);
    }
    
    public static void MoveToNewMapP2(final GamingMap WinningMap) {
        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                OpChat.SendMessageToAllOps(GPD.getName(), "Moving everyone to " + WinningMap.getName() + ".");
                RramaGaming.MapInPlay = WinningMap;
                RGAchievements.AwardAchievement(RGAchievementEvents.JoinedB4Start, RGAchievements.Achievement.Start_At_The_Start);
                RGAchievementEvents.JoinedB4Start.clear();
                GPD.callStartPreGame();
            }
        }, 8*20);
    }
    
    public static class GetVotes implements TabExecutor {
        
        @Override
        public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLable, String[] args) {
            return Arrays.asList("1", "2");
        }
        
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("Vote")) {
                if (RramaGaming.R != RoundState.Voting) {
                    sender.sendMessage(ChatColor.YELLOW + "Map voting is not in progress.");
                    return true;
                } else if (args.length == 1) {
                    if (args[0].equals("1") || args[0].equals("2")) {
                        if (HasVoted.contains(sender)) {
                            sender.sendMessage(ChatColor.YELLOW + "You have already voted.");
                        } else {
                            AddVote(args[0]);
                            HasVoted.add(sender);
                            sender.sendMessage(ChatColor.YELLOW + "Thank you for voting.");
                            if (sender instanceof Player) {
                                RGAchievements.AwardAchievement(((Player)sender).getName(), Achievement.Democracy);
                            }
                        }
                        return true;
                    } else if (args[0].equalsIgnoreCase("skip")) {
                        if (sender.hasPermission("rramagaming.voteforce")) {
                            VoteTask.cancel();
                            OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " skipped the map vote.");
                            Vote.GetResults();
                        } else {
                            sender.sendMessage(ChatColor.YELLOW + "You do not have the required permissions to skip the vote.");
                        }
                        return true;
                    } else return false;
                } else if (args.length == 2) {
                    if (args[1].equalsIgnoreCase("rigged")) {
                        if (sender.hasPermission("rramagaming.voteforce")) {
                            if (args[0].equals("1") || args[0].equals("2")) {
                                sender.sendMessage(ChatColor.YELLOW + "Thank you for voting.");
                                for (int i = 0; i < 100; ++i) {
                                    AddVote(args[0]);
                                }
                            } else {
                                sender.sendMessage(ChatColor.YELLOW + "You can only vote for map 1 or 2.");
                            }
                        } else {
                            sender.sendMessage(ChatColor.YELLOW + "You do not have the required permissions to rig the vote.");
                        }
                        return true;
                    } else return false;
                } else return false;
            } else return false;
        }
    }
}