package me.rrama.RramaGaming;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitTask;

public class FreezeStalkPossess implements Listener {
    
    public static ArrayList<String> Frozen = new ArrayList<>();
    public static int PlayersPossessed;
    public static BukkitTask MoveEvent;
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        
        Player P = event.getPlayer();
        String PN = P.getName();
        if (Frozen.contains(PN)) {
            event.setCancelled(true);
        }
    }
    
    public static void MoveToMaster() {
        MoveEvent = Bukkit.getScheduler().runTaskTimer(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                for (Player P : Bukkit.getOnlinePlayers()) {
                    Gamer G = RramaGaming.getGamer(P.getName());
                    String MasterN = G.getSlavedTo();
                    if (MasterN != null) {
                        Player Master = Bukkit.getPlayer(MasterN);
                        if (Master != null) {
                            P.setFlying(Master.isFlying());
                            P.setSneaking(Master.isSneaking());
                            P.setSprinting(Master.isSprinting());
                            P.teleport(Master.getLocation());
                        } else {
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "Possess " + P.getName() + " off");
                        }
                    }
                }
            }
            
        }, 1, 1);
    }
    
    public static class CMDs implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("Freeze") && args.length == 1) {
                Player victim = Bukkit.getPlayer(args[0]);
                if (victim == null) {
                    sender.sendMessage(ChatColor.YELLOW + "Cannot find the player spesified.");
                    return true;
                }
                String victimN = victim.getName();
                if (Frozen.contains(victimN)) {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " defrosted " + victim.getName() + ".");
                    victim.sendMessage(ChatColor.GOLD + "You have been defrosted.");
                    victim.setFlying(false);
                    Frozen.remove(victimN);
                } else {
                    OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " froze " + victim.getName() + ".");
                    victim.sendMessage(ChatColor.GOLD + "You have been frozen.");
                    victim.setFlying(true);
                    Frozen.add(victimN);
                }
                return true;
                
            } else if (commandLable.equalsIgnoreCase("Possess") && args.length == 2) {
                Player victim = Bukkit.getPlayer(args[0]);
                if (victim == null) {
                    sender.sendMessage(ChatColor.YELLOW + "Cannot find the player spesified.");
                    return true;
                }
                String victimN = victim.getName();
                String SN = sender.getName();
                if (args[1].equalsIgnoreCase("on")) {
                    if (sender instanceof Player) {
                        Player PS = (Player)sender;
                        if (PS == victim) {
                            PS.sendMessage(ChatColor.YELLOW + "You cannot possess yourself.");
                        } else {
                            Gamer G = RramaGaming.getGamer(victimN);
                            boolean b = G.setSlavedTo(SN);
                            if (b) {
                                OpChat.SendMessageToAllOps("RramaGaming", SN + " possessed " + victimN + ".");
                                if (PlayersPossessed == 0) {
                                    MoveToMaster();
                                }
                                PlayersPossessed++;
                            } else {
                                PS.sendMessage(ChatColor.YELLOW + G.getSlavedTo() + " has already possessed " + victimN + ".");
                            }
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You need to be a player to possess someone.");
                    }
                    return true;
                } else if (args[1].equalsIgnoreCase("off")) {
                    Gamer G = RramaGaming.getGamer(victimN);
                    if (G.getSlavedTo() != null) {
                        OpChat.SendMessageToAllOps("RramaGaming", SN + " made " + victimN + " not possessed.");
                        G.setSlavedTo(null);
                        PlayersPossessed = PlayersPossessed - 1;
                        if (PlayersPossessed == 0) {
                            MoveEvent.cancel();
                        }
                        victim.setFlying(false);
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + victimN + " is already un-possessed.");
                    }
                    return true;
                } else return false;
                
            } else if ((commandLable.equalsIgnoreCase("Stalk") || commandLable.equalsIgnoreCase("Follow")) && args.length == 1) {
                Player victim = Bukkit.getPlayer(args[0]);
                if (victim == null) {
                    sender.sendMessage(ChatColor.YELLOW + "Cannot find the player spesified.");
                    return true;
                }
                String victimN = victim.getName();
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.YELLOW + "You need to be a player to stalk someone.");
                    return true;
                }
                String SN = sender.getName();
                Player PS = (Player)sender;
                Gamer G = RramaGaming.getGamer(SN);
                if (G.getSlavedTo() == null) {
                    if (PS == victim) {
                        PS.sendMessage(ChatColor.YELLOW + "You cannot possess yourself.");
                    } else {
                        boolean b = G.setSlavedTo(victimN);
                        sender.sendMessage(ChatColor.YELLOW + "You are now following " + victimN + ".");
                        if (PlayersPossessed == 0) {
                            MoveToMaster();
                        }
                        PlayersPossessed++;
                    }
                    return true;
                } else {
                    G.setSlavedTo(null);
                    sender.sendMessage(ChatColor.YELLOW + "You are no longer following " + victimN + ".");
                    PlayersPossessed = PlayersPossessed - 1;
                    if (PlayersPossessed == 0) {
                        MoveEvent.cancel();
                    }
                    return true;
                }
            } else return false;
        }
    }
}