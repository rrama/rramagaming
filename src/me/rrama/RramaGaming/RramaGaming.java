package me.rrama.RramaGaming;

/**
 *
 * @author rrama
 * All rights reserved to rrama.
 * No copying/stealing any part of the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * No copying/stealing ideas from the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * 
 * @credits Credit goes to rrama (author), WorldEdit Team (Use of WorldEdit API).
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Handler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class RramaGaming extends JavaPlugin {
    
    protected static Random Random = new Random();
    public static ArrayList<String> EnabledGames = new ArrayList<>(), DisabledGames = new ArrayList<>();
    private static HashMap<String, Gamer> PN2G = new HashMap<>();
    protected static Plugin ThisPlugin;
    public static String gameInPlay;
    public static RoundState R;
    public static ArrayList<String> WasInRound = new ArrayList<>();
    public static GamingMap MapInPlay;
    public static final String RGTagNo = ChatColor.BLACK + "[RramaGaming] "; //Tag for logs and shizz. W/out an end colour.
    public static final String RGTagB = RGTagNo + ChatColor.BLUE; //Tag for logs and shizz. W/ Blue as an end colour.
    
    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelTasks(ThisPlugin);
        for (Player P : Bukkit.getOnlinePlayers()) {
            RevealPlayerToAll(P);
            P.setPlayerListName(null);
        }
        SpoutGUI.unGUI();
        if (ServerState.getServerState() == ServerState.Reload) {
            FolderSetup.writeConsoleViewers();
        } else {
            File F = new File(FolderSetup.pluginFolder, "State.txt");
            F.delete();
        }
    }

    @Override
    public void onEnable() {
        R = RoundState.Off;
        gameInPlay = "null";
        ThisPlugin = this;
        FolderSetup.FolderSetUp();
        MapInPlay = GamingMaps.GamingMaps.get("Spawn");
        for (Player P: Bukkit.getOnlinePlayers()) {
            addGamer(P.getName(), new Gamer(P, TextureCommand.FileAllowsTextures(P)));
        }
        
        FolderSetup.readConsoleViewers();
        for (Handler H : Bukkit.getLogger().getHandlers()) {
            if (H.toString().equals("CommandAsConsole Logger")) {
                Bukkit.getLogger().removeHandler(H);
            }
        }
        Bukkit.getLogger().addHandler(new CommandAsConsole());
        
        RainGold.ScheduleNewGoldRain();
        MoonPhaseChangeEvent.MoonCheck();
        RGRegisterEvents.Register();
        
        getCommand("CommandAsConsole").setExecutor(new CommandAsConsole());
        getCommand("ViewConsole").setExecutor(new CommandAsConsole());
        getCommand("Refs").setExecutor(new Ref());
        getCommand("Ref").setExecutor(new Ref());
        getCommand("Hide").setExecutor(new Ref());
        getCommand("Vote").setExecutor(new Vote.GetVotes());
        getCommand("ResetMap").setExecutor(new MapReset.ResetMapCMD());
        getCommand("Goto").setExecutor(new Goto());
        getCommand("GrabGold").setExecutor(new RainGold());
        getCommand("Spawn").setExecutor(new Goto());
        getCommand("Players").setExecutor(new Players());
        getCommand("Time").setExecutor(new TimeCommands());
        getCommand("AddTime").setExecutor(new TimeCommands());
        getCommand("Shop").setExecutor(new Shop());
        getCommand("Cookies").setExecutor(new Cookies());
        getCommand("SetCookies").setExecutor(new Cookies());
        getCommand("AddCookies").setExecutor(new Cookies());
        getCommand("SendCookies").setExecutor(new Cookies());
        getCommand("Achievements").setExecutor(new RGAchievementCommands());
        getCommand("LoadedGames").setExecutor(new StartAndStopGame());
        getCommand("UnloadedGames").setExecutor(new StartAndStopGame());
        getCommand("LoadGame").setExecutor(new StartAndStopGame());
        getCommand("UnloadGame").setExecutor(new StartAndStopGame());
        getCommand("GameStart").setExecutor(new StartAndStopGame());
        getCommand("GameStop").setExecutor(new StartAndStopGame());
        getCommand("EndRound").setExecutor(new StartAndStopGame());
        getCommand("High5").setExecutor(new High5());
        getCommand("Hacks").setExecutor(new Hacks());
        getCommand("Freeze").setExecutor(new FreezeStalkPossess.CMDs());
        getCommand("Possess").setExecutor(new FreezeStalkPossess.CMDs());
        getCommand("Stalk").setExecutor(new FreezeStalkPossess.CMDs());
        getCommand("GarbageCollection").setExecutor(new GarbageCollection());
        getCommand("DrumNoise").setExecutor(new DrumNoise());
        getCommand("FunVote").setExecutor(new FunVote());
        getCommand("FunVoteCast").setExecutor(new FunVote());
        getCommand("SlapWith").setExecutor(new SlapWith());
        getCommand("Facepalm").setExecutor(new Facepalm());
        getCommand("OpChat").setExecutor(new OpChat());
        getCommand("Title").setExecutor(new TitleCMDs());
        getCommand("TitleColour").setExecutor(new TitleCMDs());
        getCommand("NameColour").setExecutor(new TitleCMDs());
        getCommand("Colours").setExecutor(new TitleCMDs());
        getCommand("Whois").setExecutor(new Whois());
        getCommand("WhoWas").setExecutor(new Whois());
        getCommand("UserNotes").setExecutor(new UserNotes());
        getCommand("AddUserNotes").setExecutor(new UserNotes());
        getCommand("Textures").setExecutor(new TextureCommand());
    }
    
    public static void RemoveAllMonsters() {
        for (World w : Bukkit.getWorlds()) {
            for (Entity e : w.getEntities()) {
                if (e instanceof Monster) {
                    e.remove();
                }
            }
        }
    }
    
    public static void teleportAllPlayersTo(final Location l) {
        for (Player p : Bukkit.getWorlds().get(0).getPlayers()) {
            p.teleport(l);
        }
    }
    
    public static void RevealPlayerToAll(Player P) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.showPlayer(P);
        }
    }
    
    public static boolean MoreAvalibleGamersThan(final int i) {
        int i1 = 0;
        for (Gamer G : getGamers()) {
            if (G.m() && !G.isRef()) i1++;
        }
        return (i1 >= i);
    }
    
    public static ArrayList<Gamer> getGamersWhoCanPlay() {
        ArrayList<Gamer> GamersWhoCanPlay = new ArrayList<>();
        for (Gamer G : getGamers()) {
            if (G.m() && !G.isRef()) {
                GamersWhoCanPlay.add(G);
            }
        }
        return GamersWhoCanPlay;
    }
    
    public static Collection<Gamer> getGamers() {
        return PN2G.values();
    }
    
    public static Gamer getGamer(final String PN) {
        return PN2G.get(PN);
    }
    
    public static void addGamer(final String PN, final Gamer CP) {
        PN2G.put(PN, CP);
    }
    
    public static void removeGamer(final String PN) {
        PN2G.remove(PN);
    }
    
}