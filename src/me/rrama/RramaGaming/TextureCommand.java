package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TextureCommand implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Textures")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.YELLOW + "Only players have texture packs. DW you still look good in anything.");
                return true;
            }
            Player P = (Player) sender;
            Gamer G = RramaGaming.getGamer(P.getName());
            if (args.length == 0) {
                G.Textures = !G.Textures;
            } else if (args.length < 1 || !(args[0].equalsIgnoreCase("False") || args[0].equalsIgnoreCase("True"))) {
                return false;
            } else {
                G.Textures = Boolean.valueOf(args[0].toLowerCase());
            }
            UpdateTextureFile(G);
            if (G.Textures) {
                P.sendMessage(ChatColor.YELLOW + "You've set your texture pack prefrences to allow changing textures.");
            } else {
                P.sendMessage(ChatColor.YELLOW + "You've set your texture pack prefrences to disallow changing textures.");
            }
            return true;
        } else return false;
    }
    
    public static boolean FileAllowsTextures(Player P) {
        File F = new File(FolderSetup.TexturePackFolder, P.getName() + ".txt");
        try (Scanner s = new Scanner(F)) {
            String BS = s.nextLine();
            return (Boolean.valueOf(BS));
        } catch (FileNotFoundException ex) {
            FolderSetup.CreateFile(FolderSetup.TexturePackFolder, P.getName() + ".txt", RramaGaming.RGTagB, "true");
            return FileAllowsTextures(P);
        }
    }
    
    public static void UpdateTextureFile(Gamer G) {
        File F = new File(FolderSetup.TexturePackFolder, G.getName() + ".txt");
        try (FileWriter FW = new FileWriter(F)) {
            FW.write(String.valueOf(G.Textures));
            FW.close();
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, null, ex);
        }
    }
}
