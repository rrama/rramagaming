package me.rrama.RramaGaming;

import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class HealthyFood implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            event.setCancelled(true);
            final Player P = (Player)event.getEntity();
            
            final int DoggyBag = event.getFoodLevel() - 7;
            if (DoggyBag > 0) {
                final int EatenDoggyBag = DoggyBag + P.getHealth();
                if (EatenDoggyBag >= P.getMaxHealth()) {
                    P.setHealth(P.getMaxHealth());
                } else {
                    P.setHealth(EatenDoggyBag);
                }
                RGAchievements.AwardAchievement(P.getName(), Achievement.Eater);
            }
            
            P.setFoodLevel(7);
        }
    }
}