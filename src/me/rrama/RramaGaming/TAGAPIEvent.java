package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.kitteh.tag.PlayerReceiveNameTagEvent;
import org.kitteh.tag.TagAPI;

public class TAGAPIEvent implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerReceiveNameTag(PlayerReceiveNameTagEvent event) {
        String PN = event.getNamedPlayer().getName();
        event.setTag(RramaGaming.getGamer(PN).getTagColour() + PN);
    }
    
    public static void refreshPlayer(final Player P) throws ClassNotFoundException {
        Bukkit.getScheduler().runTask(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                TagAPI.refreshPlayer(P);
            }
        });
    }
}