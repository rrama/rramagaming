package me.rrama.RramaGaming;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Ref implements CommandExecutor {
    
    public static ArrayList<String> Refs = new ArrayList<>();
    public static ArrayList<String> HiddenRefs = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {

        if (commandLable.equalsIgnoreCase("Hide")) {
            if (sender instanceof Player) {
                Player PS = (Player)sender;
                String PSN = PS.getName();
                if (Refs.contains(PSN)) {
                    if (HiddenRefs.contains(PSN)) {
                        RramaGaming.RevealPlayerToAll(PS);
                        sender.sendMessage(ChatColor.YELLOW + "You are now visable to everyone.");
                    } else {
                        ArrayList<String> NonRefs = new ArrayList<>();
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            NonRefs.add(p.getName());
                        }
                        NonRefs.removeAll(Refs);
                        for (String S : NonRefs) {
                            Player p = Bukkit.getPlayerExact(S);
                            p.hidePlayer(PS);
                        }
                        HiddenRefs.add(PSN);
                        sender.sendMessage(ChatColor.YELLOW + "You are now invisable.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You need to be a Ref to use this command.");
                }
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You need to be a Player to use this command.");
            }
            return true;

        } else if (commandLable.equalsIgnoreCase("Refs")) {
            if (Refs.isEmpty() == true) {
                sender.sendMessage(ChatColor.YELLOW + "There are currently no Refs online");
            } else {
                String Names = ListToString.ListToString(Refs, ", ");
                sender.sendMessage(ChatColor.YELLOW + "Refs:" + ChatColor.GOLD + Names);
            }
            return true;
            
        } else if (commandLable.equalsIgnoreCase("Ref")) {
            if (sender instanceof Player) {
                Player ps = (Player)sender;
                String pN = ps.getName();
                if (!(Refs.contains(pN))) {
                    Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.GOLD + pN + ChatColor.BLUE + " is now Reffing.");
                    ps.setPlayerListName(ChatColor.GOLD + pN);
                    Refs.add(pN);
                    RramaGaming.getGamer(pN).setRef(true);
                    if (ps.getGameMode() != GameMode.CREATIVE) {
                        ps.setGameMode(GameMode.CREATIVE);
                    }
                } else {
                    Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.WHITE + pN + ChatColor.BLUE + " is no longer Reffing.");
                    ps.setPlayerListName(null);
                    Refs.remove(pN);
                    RramaGaming.getGamer(pN).setRef(false);
                }
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You have to be a player to be a ref.");
            }
            return true;
            
        } else return false;
    }
}
