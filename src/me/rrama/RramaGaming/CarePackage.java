package me.rrama.RramaGaming;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Chest;
import org.bukkit.scheduler.BukkitTask;

public class CarePackage implements Listener {
    
    public static ArrayList<String> CarePackagers = new ArrayList<>();
    public static ArrayList<BukkitTask> SmokerTasks = new ArrayList<>();
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onCarePackagePlace(PlayerInteractEvent event) {
        Player P = event.getPlayer();
        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK) && (CarePackagers.contains(P.getName()))) {
            BlockFace BF = event.getBlockFace();
            Location l = event.getClickedBlock().getLocation();
            if (l.getBlockY() > MaxHeight.BuildHeight) {
                P.sendMessage(ChatColor.RED + "Cant place CarePackages at the Max build height." + MaxHeight.BuildHeight);
            } else if (l.getBlock().getTypeId() == 7) {
                P.sendMessage(ChatColor.RED + "Cant place CarePackages on bedrock.");
            } else {
                P.sendMessage(ChatColor.GOLD + "You have called in your Care Package.");
                RGAchievements.AwardAchievement(P.getName(), RGAchievements.Achievement.Gift_From_Heaven);
                PlaceAndRun(l, P, BF);
                CarePackagers.remove(P.getName());
            }
        }
    }
    
    public static void PlaceAndRun(Location l, final Player P, final BlockFace BF) {
        Chest C;
        if (BF == BlockFace.NORTH) {
            l.add(-1, 0, 0);
            C = new Chest(BlockFace.SOUTH);
        } else if (BF == BlockFace.SOUTH) {
            l.add(1, 0, 0);
            C = new Chest(BlockFace.NORTH);
        } else if (BF == BlockFace.EAST) {
            l.add(0, 0, -1);
            C = new Chest(BlockFace.WEST);
        } else if (BF == BlockFace.WEST) {
            l.add(0, 0, 1);
            C = new Chest(BlockFace.EAST);
        } else {
            l.add(0, 1, 0);
            C = new Chest();
        }
        Location Top = l.clone();
        Top.setY(MaxHeight.BuildHeight);
        FallingBlock FB = l.getWorld().spawnFallingBlock(Top, 54, C.getData());
        FB.setDropItem(false); //If it does land on a block that breaks it then death.
        QueSmokeAndCheckLanding(FB.getEntityId(), P.getName(), l);
    }
    
    public static Location getTopBlock(final Location l) {
        Block Y = l.getWorld().getHighestBlockAt(l);
        while (Y.isLiquid() || Y.getTypeId() == 0) {
            Y = Y.getRelative(0, -1, 0);
        }
        return Y.getLocation().add(0, 1, 0);
    }
    
    public static void QueSmokeAndCheckLanding(final int FallingPackageID, final String PN, final Location l) {
        final Location TopBlock = getTopBlock(l);
        final int Id = SmokerTasks.size(); //Using an ArrayList to stop warnings of unassigned variables. Better way may be needed.
        SmokerTasks.add(Id, Bukkit.getScheduler().runTaskTimer(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                l.getWorld().playEffect(l, Effect.SMOKE, BlockFace.UP);
                if (TopBlock.getBlock().getTypeId() != 0) {
                    Block block = TopBlock.getBlock();
                    if (block.getTypeId() != 54) { //TopBlockChanged
                        SmokerTasks.get(Id).cancel();
                        QueSmokeAndCheckLanding(FallingPackageID, PN, l);
                    } else {
                        SmokerTasks.get(Id).cancel();
                        setContent(TopBlock);
                    }
                }
            }
        }, 0, 5));
    }
    
    public static void setContent(final Location l) { //Later change to be chosen by the games.
        int I = RramaGaming.Random.nextInt(11);
        String S;
        if (I == 0) {
            S = "GREEN_RECORD";
        } else if (I == 1) {
            S = "GOLD_RECORD";
        } else {
            S = "RECORD_" + String.valueOf(I);
        }
        ItemStack IS1 = new ItemStack(Material.getMaterial(S), 1);
        
        ItemStack IS2 = new ItemStack(Material.JUKEBOX, 1);
        
        org.bukkit.block.Chest C = (org.bukkit.block.Chest)l.getBlock().getState();
        C.getBlockInventory().addItem(IS1);
        C.getBlockInventory().addItem(IS2);
    }
}