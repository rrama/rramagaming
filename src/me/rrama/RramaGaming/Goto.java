package me.rrama.RramaGaming;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Goto implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {

        if ((commandLable.equalsIgnoreCase("GoTo") || commandLable.equalsIgnoreCase("G")) && args.length == 1) {
            if (sender instanceof Player) {
                Player PS = (Player)sender;
                String MapName = args[0];
                if (GamingMaps.GamingMaps.keySet().contains(MapName)) {
                    Location l = GamingMaps.GamingMaps.get(MapName).getSpawn();
                    sender.sendMessage(ChatColor.YELLOW + "You are now being taken to " + MapName + "'s spawn.");
                    PS.teleport(l);
                    return true;
                } else return false;
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You must be a player to use this command.");
                return true;
            }
        } else if (commandLable.equalsIgnoreCase("Spawn")) {
            if (sender instanceof Player) {
                Player P = (Player)sender;
                if (RramaGaming.R == RoundState.InGame && !(Ref.Refs.contains(P.getName()))) {
                    sender.sendMessage(ChatColor.YELLOW + "Command is reserved for refs.");
                } else {
                    P.teleport(RramaGaming.MapInPlay.getSpawn());
                }
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You must be a Player to use this command.");
            }
            return true;
        } else {
            return false;
        }
    }
}