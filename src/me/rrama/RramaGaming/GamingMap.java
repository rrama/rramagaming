package me.rrama.RramaGaming;

import com.sk89q.worldedit.Vector;
import java.util.HashMap;
import org.bukkit.Location;

public final class GamingMap {

    final protected String texture;
    final protected String name;
    final protected Location spawn;
//    final MapView MV;
    final protected Vector resetpoint;
    final protected HashMap<String, Object> metadata = new HashMap<>();

    GamingMap(final String Name, final Location Spawn, final int CenterX, final int CenterZ, final Vector ResetPoint, final String Texture) {
        name = Name;
        spawn = Spawn;
//        MV = Bukkit.createMap(Bukkit.getWorlds().get(0));
//        MV.setCenterX(CenterX);
//        MV.setCenterZ(CenterZ);
//        MV.setScale(MapView.Scale.CLOSEST);
        resetpoint = ResetPoint;
        texture = Texture;
    }

    public String getName() {
        return name;
    }

    public Location getSpawn() {
        return spawn;
    }
    
//    public MapView getMapView() {
//        return MV;
//    }

    public Vector getResetPoint() {
        return resetpoint;
    }
    
    public String getTexture() {
        return texture;
    }
    
    public Object getMetaData(String key) {
        return metadata.get(key);
    }
    
    @Deprecated
    public HashMap<String, Object> getMetaData() {
        return metadata;
    }

    public void addMetaData(final String S, final Object O) {
        metadata.put(S, O);
    }

    public void removeMetaData(final String S) {
        metadata.remove(S);
    }
    
    public void resetMap() {
        MapReset.ResetMap(this);
    }
}
