package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class FolderSetup {
    
    public static FileConfiguration FC;
    public static boolean CPUOverRAM;
    public static File pluginFolder, PlayersCookiesFolder, PlayersAchievementsFolder, SchematicsFolder,
            TitlesFolder, TitleColoursFolder, PlayerColoursFolder, UserNotesFolder, TexturePackFolder;
    public static final String FS = System.getProperty("file.separator");
    public static final String LSep = System.getProperty("line.separator");
    
    public static void FolderSetUp() {
        DeleteOldMaps();
        pluginFolder = RramaGaming.ThisPlugin.getDataFolder();
        FC = RramaGaming.ThisPlugin.getConfig();
        if (pluginFolder.exists() == false) {
            boolean b = pluginFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "DataFolder failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a DataFolder for you :). rrama do good?"));
            }
            FolderSetUp();
        } else {
            File F = new File(FolderSetup.pluginFolder, "State.txt");
            if (F.exists()) {
                if (ServerState.getServerState() != ServerState.Reload) {
                    ServerState.setServerState(ServerState.Crash);
                }
            } else {
                ServerState.setServerState(ServerState.Normal);
            }
            FileSetup();
            ConfigSetup();
        }
    }
    
    public static void DeleteOldMaps() {
        for (File F : Bukkit.getWorlds().get(0).getWorldFolder().listFiles()) {
            if (F.getName().startsWith("map_")) {
                F.delete();
            }
        }
    }
    
    public static void FileSetup() {
        boolean repeat = false;
        File FileGamingMapInfo = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "GamingMapInfo.txt", RramaGaming.RGTagB, "#Format: Map name | Spawn coords | Center of the map XY | Resetpoint | Texture Pack URL" + LSep
                + "#Example: CoolMap_YEAH | 1.0, 70.5, -10.0, 90, 0 | 10, -46 | 1, 7, -30 | null");
        if (FileGamingMapInfo != null) {
            GamingMaps.ReadGamingMapInfo(FileGamingMapInfo);
        }

        PlayersAchievementsFolder = new File(pluginFolder, FS + "Achievements");
        if (PlayersAchievementsFolder.exists() == false) {
            boolean b = PlayersAchievementsFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'Achievements' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created an Achievements folder for you :). rrama do good?"));
            }
            repeat = true;
        } else {
            for (Achievement A : Achievement.values()) {
                CreateFile(PlayersAchievementsFolder, A + ".txt", RramaGaming.RGTagB, null);
            }
        }

        PlayersCookiesFolder = new File(pluginFolder, FS + "Cookies");
        if (PlayersCookiesFolder.exists() == false) {
            boolean b = PlayersCookiesFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'Cookies' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a Cookies folder for you :). rrama do good? Umm Cookies."));
            }
            repeat = true;
        }

        SchematicsFolder = new File(pluginFolder, FS + "Schematics");
        if (SchematicsFolder.exists() == false) {
            boolean b = SchematicsFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'Schematics' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a Schematics folder for you :). rrama do good? Fill with schematics now."));
            }
            repeat = true;
        }

        UserNotesFolder = new File(pluginFolder, FS + "UserNotes");
        if (UserNotesFolder.exists() == false) {
            boolean b = UserNotesFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'UserNotes' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a UserNotes folder for you :). rrama do good?"));
            }
            repeat = true;
        }

        TexturePackFolder = new File(pluginFolder, FS + "TexturePack");
        if (TexturePackFolder.exists() == false) {
            boolean b = TexturePackFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'TexturePack' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a TexturePack folder for you :). rrama do good?"));
            }
            repeat = true;
        }

        TitlesFolder = new File(pluginFolder, FS + "Titles");
        if (TitlesFolder.exists() == false) {
            boolean b = TitlesFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'Titles' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a Titles folder for you :). rrama do good?"));
            }
            repeat = true;
        }

        TitleColoursFolder = new File(pluginFolder, FS + "TitleColours");
        if (TitleColoursFolder.exists() == false) {
            boolean b = TitleColoursFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'TitleColours' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a TitleColours folder for you :). rrama do good?"));
            }
            repeat = true;
        }

        PlayerColoursFolder = new File(pluginFolder, FS + "PlayerColours");
        if (PlayerColoursFolder.exists() == false) {
            boolean b = PlayerColoursFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Folder 'PlayerColours' failed to be made."));
            } else {
                Bukkit.getLogger().info((RramaGaming.RGTagB + "Created a PlayerColours folder for you :). rrama do good?"));
            }
            repeat = true;
        }

        File FileItemPrices = CreateFile(pluginFolder, "ItemPrices.txt", RramaGaming.RGTagB, "1 RankUp: 1000" + LSep + "1 Title: 100" + LSep + "1 Title Colour: 100" + LSep + "1 Name Colour: 100");
        if (FileItemPrices != null) {
            Shop.ItemPricesReg(FileItemPrices);
        }

        File FileCensors = CreateFile(pluginFolder, "Censors.txt", RramaGaming.RGTagB, "#Censored words go in this file along with their replacement." + LSep +
        "#Format: [Censored word] = [Word to be replaced with]" + LSep +
        "#Caps do matter. The order of the censors is the order in which they will be replaced." + LSep +
        "#Each censor must go on a new line." + LSep + "#" + LSep +
        "#Example:" + LSep +
        "#Dog = Apple" + LSep +
        "#Cat = Mouse" + LSep +
        "#You can delete these comments if you so wish.");
        if (FileCensors != null) {
            try {
                Scanner s = new Scanner(FileCensors);
                s.reset();
                while (s.hasNextLine()) {
                    String S = s.nextLine();
                    if (!(S.startsWith("#"))) {
                        String[] Ss = S.split(" = ");
                        if (Ss.length == 2) {
                            Chat.CencoredList.put(Ss[0], Ss[1]);
                        } else {
                            Bukkit.getLogger().warning((RramaGaming.RGTagB + "Line: '" + S + "' in File 'Censors.txt' does not contain exactly 2 strings."));
                        }
                    }
                }
            } catch (FileNotFoundException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not find File 'Censors.txt' even though it exists."));
            }
        }

        if (repeat) {
            FileSetup();
        }
    }
    
    public static void readConsoleViewers() {
        File F = new File(pluginFolder, "ConsoleViewers.txt");
        try (Scanner S = new Scanner(F)) {
            for (String GN : S.nextLine().split(", ")) {
                RramaGaming.getGamer(GN).setViewingConsole(true);
            }
            S.close();
            F.delete();
        } catch (FileNotFoundException ex) {}
    }
    
    public static void writeConsoleViewers() {
        File F = new File(pluginFolder, "ConsoleViewers.txt");
        if (F.exists()) {
            F.delete();
        }
        CreateFile(pluginFolder, "ConsoleViewers.txt", RramaGaming.RGTagB, CommandAsConsole.getConsoleViewers());
    }
    
    public static void ConfigSetup() {
        if (!FC.isSet("CPU over RAM")) {
            FC.set("CPU over RAM", true);
        }
        CPUOverRAM = FC.getBoolean("CPU over RAM");
        
        RramaGaming.ThisPlugin.saveConfig();
        RramaGaming.ThisPlugin.reloadConfig();
    }
    
    public static File CreateFile(final File Folder, final String FileN, final String TagB, final String ToWrite) {
        File file = new File(Folder, FS + FileN);
        if (file.exists() == false) {
            try {
                boolean b = file.createNewFile();
                if (b == false) {
                    Bukkit.getLogger().warning((TagB + "File '" + FileN + "' failed to be made."));
                } else {
                    if (ToWrite == null) {
                        Bukkit.getLogger().info((TagB + "Created '" + FileN + "' for you :). rrama do good?"));
                    } else {
                        try (FileWriter FW = new FileWriter(file)) {
                            FW.write(ToWrite);
                            FW.close();
                            Bukkit.getLogger().info((TagB + "Created '" + FileN + "' for you :). rrama do good?"));
                        } catch (Exception e) {
                            Bukkit.getLogger().warning((TagB + "Failed to write to '" + FileN + "'."));
                        }
                    }
                }
                return CreateFile(Folder, FileN, TagB, ToWrite);
            } catch (IOException ex) {
                Bukkit.getLogger().warning((TagB + "'IOException' File '" + FileN + "' failed to be made in '" + Folder + "'."));
                return null;
            }
        } else return file;
    }
}
