package me.rrama.RramaGaming;

import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

public class JoinAndQuit1 implements Listener {
    
    public static BukkitTask WaitingForPlayersRepeator;
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLogin(PlayerLoginEvent event) {
        
        if (event.getResult() == PlayerLoginEvent.Result.ALLOWED) {
            Player P = event.getPlayer();
            PlayersFiles(P);
            Gamer G = new Gamer(P, TextureCommand.FileAllowsTextures(P));
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {

        Player P = event.getPlayer();
        if (RramaGaming.gameInPlay.equals("null")) {
            P.sendMessage(ChatColor.GOLD + "No game is in play.");
        } else {
            P.sendMessage(ChatColor.GOLD + RramaGaming.gameInPlay + " is in play.");
            if (RramaGaming.R == RoundState.Voting) {
                P.sendMessage(ChatColor.GOLD + "Map vote in progress.");
                P.sendMessage(ChatColor.BLACK + "[" + RramaGaming.gameInPlay + "] " + ChatColor.GOLD + "Map Vote:");
                P.sendMessage(ChatColor.GOLD + "Map 1: " + ChatColor.RED + Vote.map1.getName() + ChatColor.GOLD + " Map 2: " + ChatColor.GREEN + Vote.map2.getName());
                P.sendMessage(ChatColor.GOLD + "Vote with " + ChatColor.YELLOW + "/vote " + ChatColor.RED + "1 " + ChatColor.YELLOW + "or " + ChatColor.GREEN + "2" + ChatColor.GOLD + ".");
            }
        }
        if (!RramaGaming.WasInRound.contains(P.getName()) || !(RramaGaming.R == RoundState.InGame || RramaGaming.R == RoundState.MapReset)) {
            P.teleport(RramaGaming.MapInPlay.getSpawn());
            P.setHealth(P.getMaxHealth());
            RramaGaming.WasInRound.add(P.getName());
        }
        P.setGameMode(GameMode.SURVIVAL);
        final Gamer G = RramaGaming.getGamer(P.getName());
        G.resetToPlay();
        try {
            G.sendTexturePack(GamingMaps.checkTextureURL(RramaGaming.MapInPlay.getTexture()));
        } catch (NullPointerException ex) {}
        try {
            G.updateCookies();
        } catch (CookieFileException ex) {
            ex.sendConsoleMessage();
            ex.sendPlayerMessage();
        }
        if (!Bukkit.dispatchCommand(P, "Cookies")) {
            P.kickPlayer("You have a broken cookie file.\nReport this at http://www.forum.degneboligs.com");
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoinMonitor(PlayerJoinEvent event) {
        Player P = event.getPlayer();
        SpoutGUI.Call(P);
        if (RramaGaming.R == RoundState.NeedMorePlayers) {
            GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
            if (RramaGaming.MoreAvalibleGamersThan(GP.MinimumPlayersNeeded)) {
                WaitingForPlayersRepeator.cancel();
                StartAndStopGame.MapVote(GP);
            } else {
                P.sendMessage(ChatColor.GOLD + "Waiting for at least "+GP.MinimumPlayersNeeded+" players to start "+GP.getName()+".");
            }
        }
    }
    
    public static void WaitingForPlayersMessage() {
        WaitingForPlayersRepeator = Bukkit.getScheduler().runTaskTimerAsynchronously(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                GamingPlugin GP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                Bukkit.broadcastMessage(GP.TagNo + ChatColor.RED + "Need at least "+GP.MinimumPlayersNeeded+" players to start "+GP.getName()+".");
            }
            
        }, 2400, 2400);
    }
    
    public static void PlayersFiles(Player P) {
        File PCF = FolderSetup.CreateFile(FolderSetup.PlayersCookiesFolder, P.getName() + ".txt", RramaGaming.RGTagB, "0");
        if (!PCF.exists()) {
            P.kickPlayer("You have no cookie file.\nPlease relog, if the problem persists then report this at http://www.forum.degneboligs.com");
        }
        FolderSetup.CreateFile(FolderSetup.TexturePackFolder, P.getName() + ".txt", RramaGaming.RGTagB, "true");
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player P = event.getPlayer();
        String PN = P.getName();
        Gamer G = RramaGaming.getGamer(PN);
        
        if (Ref.Refs.contains(PN)) {
            Ref.Refs.remove(PN);
        }
        
        SpoutGUI.removeCookieLabel(PN);
        
        RramaGaming.RevealPlayerToAll(P);
        P.setPlayerListName(null);
        
        G.remove();
    }
}