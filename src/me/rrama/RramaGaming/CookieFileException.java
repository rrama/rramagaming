package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class CookieFileException extends Exception {
    
    public CookieFileProblem CFP;
    public OfflinePlayer offlineplayer;
    public OfflineGamer offlinegamer;
    
    CookieFileException(CookieFileProblem newCFP, String PlayerName) {
        CFP = newCFP;
        offlinegamer = new OfflineGamer(Bukkit.getOfflinePlayer(PlayerName));
    }
    
    CookieFileException(CookieFileProblem newCFP, OfflinePlayer offlineplayer) {
        CFP = newCFP;
        this.offlineplayer = offlineplayer;
        offlinegamer = new OfflineGamer(offlineplayer);
    }
    
    public CookieFileProblem getProblem() {
        return CFP;
    }
    
    public String getPlayerMessage() {
        if (CFP.equals(CookieFileProblem.DoesNotExist)) {
            return ("Your cookie file does not exist.");
        } else if (CFP.equals(CookieFileProblem.WasNotFound)) {
            return ("Your cookie file was not found.");
        } else if (CFP.equals(CookieFileProblem.IsCorrupt)) {
            return ("Your cookie file is corrupt.");
        } else if (CFP.equals(CookieFileProblem.IsUnwritable)) {
            return ("Your cookie file is unwritable.");
        } else if (CFP.equals(CookieFileProblem.NoSuchElement)) {
            return ("Your cookie file is empty.");
        } else {
            return ("Your cookie file has an unknown error!!!!!1!"); //never to be returned.
        }
    }
    
    public OfflineGamer getOfflineGamer() {
        return offlinegamer;
    }
    
    public OfflinePlayer getOfflinePlayer() {
        return offlineplayer;
    }
    
    public void sendPlayerMessage() {
        if (offlineplayer.isOnline()) {
            ((Player)offlineplayer).sendMessage(ChatColor.RED + getPlayerMessage());
        }
    }
    
    public String getConsoleMessage() {
        if (CFP.equals(CookieFileProblem.DoesNotExist)) {
            return (offlineplayer.getName() + "'s cookie file does not exist.");
        } else if (CFP.equals(CookieFileProblem.WasNotFound)) {
            return (offlineplayer.getName() + "'s cookie file was not found.");
        } else if (CFP.equals(CookieFileProblem.IsCorrupt)) {
            return (offlineplayer.getName() + "'s cookie file is corrupt.");
        } else if (CFP.equals(CookieFileProblem.IsUnwritable)) {
            return (offlineplayer.getName() + "'s cookie file is unwritable.");
        } else if (CFP.equals(CookieFileProblem.NoSuchElement)) {
            return (offlineplayer.getName() + "'s cookie file is empty.");
        } else {
            return (offlineplayer.getName() + "'s cookie file has an unknown error!!!!!1!"); //never to be returned.
        }
    }
    
    public void sendConsoleMessage() {
        Bukkit.getLogger().warning(getConsoleMessage());
    }
    
    public static enum CookieFileProblem {
        /**
         * When the file doesn't exist.
         */
        DoesNotExist,
        
        /**
         * When the file can't be found.
         */
        WasNotFound,
        
        /**
         * When the file is corrupt (Not an integer).
         */
        IsCorrupt,
        
        /**
         * When the file is unwritable.
         */
        IsUnwritable,
        
        /**
         * When the end of the Cookie file is reached with no elements being scanned.
         */
        NoSuchElement,
    }
}