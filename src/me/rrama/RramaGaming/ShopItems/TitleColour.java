package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.TitleCMDs;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class TitleColour extends ShopItem {
    
    public TitleColour(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (TitleCMDs.HasATitleColourToUse.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You already have a title colour to use. Use /TitleColour [Colour].");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        TitleCMDs.HasATitleColourToUse.add(P.getName());
        return true;
    }
    
}