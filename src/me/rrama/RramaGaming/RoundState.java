package me.rrama.RramaGaming;

public enum RoundState {
    
    Off,
    
    PreGame,
    
    InGame,
    
    AfterGame,
    
    Voting,
    
    MapReset,
    
    NeedMorePlayers;
    
}
