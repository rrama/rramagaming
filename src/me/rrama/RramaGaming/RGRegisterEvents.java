package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class RGRegisterEvents {
    
    public static void Register () {
        reg(new CommandAsConsole());
        reg(new RGAchievementEvents());
        reg(new Pain());
        reg(new ServerStateEvents());
        reg(new JoinAndQuit1());
        reg(new MonsterSpawn());
        reg(new HealthyFood());
        reg(new Chat());
        reg(new FreezeStalkPossess());
        if (Bukkit.getPluginManager().isPluginEnabled("Spout")) {
            reg(new SpoutGUIDanger());
        }
        if (Bukkit.getPluginManager().isPluginEnabled("TagAPI")) {
            reg(new TAGAPIEvent());
        }
        if (Bukkit.getPluginManager().isPluginEnabled("Votifier")) {
            reg(new Votifier());
        }
        reg(new DispenserRefill());
        reg(new TNTPlacing());
        reg(new CarePackage());
        reg(new MaxHeight());
        reg(new AliveCheck());
    }
    
    public static void reg(Listener ll) {
        Bukkit.getServer().getPluginManager().registerEvents(ll, RramaGaming.ThisPlugin);
    }
    
}
