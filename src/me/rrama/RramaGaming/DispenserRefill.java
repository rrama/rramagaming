package me.rrama.RramaGaming;

import org.bukkit.block.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class DispenserRefill implements Listener {
    
    BlockFace[] BFs = new BlockFace[] {BlockFace.DOWN, BlockFace.UP, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockDispense(BlockDispenseEvent event) {
        Block block = event.getBlock();
        for (BlockFace BF : BFs) {
            Block block2 = block.getRelative(BF);
            if (block2.getTypeId() == 63 || block2.getTypeId() == 68) {
                Sign sign = (Sign) block2.getState();
                if (sign.getLine(0).equals("[Refill]")) {
                    Dispenser d = (Dispenser) block.getState();
                    d.getInventory().addItem(event.getItem());
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSignChange(SignChangeEvent event) {
        Block block = event.getBlock();
        Sign sign = (Sign) block.getState();
        if ((sign.getLine(0).equals("[Refill]") || sign.getLine(0).equals("[Auto chest]")) && !event.getPlayer().hasPermission("rramagaming.refiller")) {
            block.setTypeId(0);
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryOpen(InventoryOpenEvent event) {
        if (event.getInventory().getType() == InventoryType.CHEST) {
            if (event.getInventory().getHolder() instanceof Chest) { //Stops double chests.
                Chest chest = (Chest) event.getInventory().getHolder();
                for (BlockFace BF : BFs) {
                    Block block2 = chest.getBlock().getRelative(BF);
                    if (block2.getTypeId() == 63 || block2.getTypeId() == 68) {
                        Sign sign = (Sign) block2.getState();
                        if (sign.getLine(0).equals("[Auto chest]")) {
                            event.setCancelled(true);
                            for (ItemStack iss : chest.getInventory()) {
                                event.getPlayer().getInventory().addItem(iss);
                            }
                        }
                    }
                }
            }
        }
    }
}
