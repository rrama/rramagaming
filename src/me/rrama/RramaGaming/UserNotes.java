package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class UserNotes implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("UserNotes") && args.length == 1) {
            OfflinePlayer OP = Bukkit.getOfflinePlayer(args[0]);
            if (OP == null) {
                sender.sendMessage(ChatColor.YELLOW + "Could not find the user spesified.");
                return true;
            }
            
            int i = 1;
            sender.sendMessage(ChatColor.YELLOW + "---- " + OP.getName() + "'s notes ----");
            File F = new File(FolderSetup.UserNotesFolder, OP.getName() + ".txt");
            try (Scanner S = new Scanner(F)) {
                while (S.hasNextLine()) {
                    sender.sendMessage(ChatColor.YELLOW + "" + i + ". " + S.nextLine());
                    i++;
                }
                S.close();
            } catch (Exception ex) {
                sender.sendMessage(ChatColor.YELLOW + "There are no notes for this user.");
            }
            try {
                if (OP.isBanned()) {
                    File BannedTxt = new File("banned-players.txt");
                    Scanner S2 = new Scanner(BannedTxt);
                    while (S2.hasNextLine()) {
                        String Raw = S2.nextLine();
                        if (Raw.startsWith(OP.getName())) {
                            String[] Raws = Raw.split("\\|");
                            String Date = Raws[1];
                            String by = Raws[2];
                            String until = Raws[3];
                            String Reason = ListToString.ListToString(Raws, "\\|", 4, 0); //To avoid a | being in the reason.
                            sender.sendMessage(ChatColor.YELLOW + "" + i + ". " + "Banned on " + Date + " by " + by + " until " + until + " for \"" + Reason + "\".");
                            break;
                        }
                    }
                }
            } catch (FileNotFoundException ex) {
                sender.sendMessage(ChatColor.YELLOW + "Can not find 'banned-players.txt'.");
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Can not find 'banned-players.txt'."));
            }
            return true;
            
        } else if (commandLable.equalsIgnoreCase("AddUserNotes") && args.length > 1) {
            OfflinePlayer OP = Bukkit.getOfflinePlayer(args[0]);
            if (OP == null) {
                sender.sendMessage(ChatColor.YELLOW + "Could not find the user spesified.");
                return true;
            }
            
            File F = new File(FolderSetup.UserNotesFolder, OP.getName() + ".txt");
            if (!F.exists()) {
                try {
                    F.createNewFile();
                } catch (IOException ex) {
                    sender.sendMessage(ChatColor.YELLOW + "Could not create notes for this user.");
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + "Unable to create user notes for " + OP.getName()));
                    return true;
                }
            }
            try (FileWriter FW = new FileWriter(F, true)) {
                String ToWrite = ListToString.ListToString(args, " ", 1, 0);
                FW.write(ToWrite);
                FW.close();
                sender.sendMessage(ChatColor.YELLOW + "Added user notes for " + OP.getName());
            } catch (IOException ex) {
                sender.sendMessage(ChatColor.YELLOW + "Unable to write to " + OP.getName() + "'s user notes.");
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Unable to write to " + OP.getName() + "'s user notes."));
            }
            return true;
            
        } else return false;
    }
}
