package me.rrama.RramaGaming;

import org.bukkit.entity.Player;

public class SpoutGUI {
    
    public static void unGUI() {
        try {
            SpoutGUIDanger.unGUI();
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {}
    }
    
    public static void Call(final Player P) {
        try {
            SpoutGUIDanger.Call(P);
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {}
    }
    
    public static void removeCookieLabel(final String PN) {
        try {
            SpoutGUIDanger.removeCookieLabel(PN);
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {}
    }
    
    public static void UpdateCookieCorner(final Gamer G) {
        try {
            SpoutGUIDanger.UpdateCookieCorner(G);
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {}
    }
}
