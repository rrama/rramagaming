package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

/**
 * Represents a Gamer.
 */
public final class Gamer extends OfflineGamer {
    
    protected boolean Alive = true;
    boolean Textures;
    boolean Reffing = false;
    boolean ViewingConsole = false;
    ChatColor TagColour = ChatColor.WHITE;
    String GamePlaying = RramaGaming.gameInPlay;
    String PreAppendedMessage = null;
    String SlavedTo = null;
    final protected HashMap<String, Object> metadata = new HashMap<>();

    Gamer(final Player P, final boolean Textures) {
        super(P);
        this.Textures = Textures;
        RramaGaming.addGamer(PN, (Gamer)this);
    }

    public Player getPlayer() {
        return Bukkit.getPlayerExact(PN);
    }

    public boolean m() {
        return (GamePlaying.equals(RramaGaming.gameInPlay));
    }

    public String getGamePlaying() {
        return GamePlaying;
    }

    public void setGamePlaying(final String GameName) {
        GamePlaying = GameName;
    }
    
    public boolean isViewingConsole() {
        return ViewingConsole;
    }
    
    public void setViewingConsole(final boolean Viewing) {
        ViewingConsole = Viewing;
    }
    
    public boolean isRef() {
        return Reffing;
    }
    
    public boolean isAlive() {
        return Alive;
    }

    public void setRef(final boolean Reffing) {
        this.Reffing = Reffing;
    }

    public void remove() {
        RramaGaming.removeGamer(PN);
    }

    public Rank getRank() {
        Player P = Bukkit.getPlayer(PN);
        if (P.hasPermission("rramagaming.admin")) {
            return Rank.Admin;
        } else if (P.hasPermission("rramagaming.moderator")) {
            return Rank.Moderator;
        } else if (P.hasPermission("rramagaming.trusted")) {
            return Rank.Trusted;
        } else if (P.hasPermission("rramagaming.donator")) {
            return Rank.Donator;
        } else if (P.hasPermission("rramagaming.advanced")) {
            return Rank.Advanced;
        } else if (P.hasPermission("rramagaming.regular")) {
            return Rank.Regular;
        } else if (P.hasPermission("rramagaming.guest")) {
            return Rank.Guest;
        } else if (P.isBanned()) {
            return Rank.Banned;
        } else {
            return Rank.Unranked;
        }
    }

    public String getPreAppendededMessage() {
        return PreAppendedMessage;
    }

    public void setPreAppendededMessage(final String Message) {
        PreAppendedMessage = Message;
    }

    public String getSlavedTo() {
        return SlavedTo;
    }

    public boolean setSlavedTo(final String PN) {
        if (SlavedTo != null && PN != null) {
            return false;
        } else {
            SlavedTo = PN;
            return true;
        }
    }
    
    public boolean sendTexturePack(String url) {
        if (Textures) {
            if (url.equals("Error")) return true;
            getPlayer().setTexturePack(url);
            return true;
        } else return false;
    }
    
    @Override
    public void updateCookies() throws CookieFileException {
        super.updateCookies();
        SpoutGUI.UpdateCookieCorner(this);
    }
    
    public void zeroExp() {
        Player P = getPlayer();
        P.setExp(0);
        P.setLevel(0);
        P.setTotalExperience(0);
    }
    
    public void removeAllPotions() {
        Player P = getPlayer();
        ArrayList<PotionEffect> ActivePotionEffects = new ArrayList<>(); //Clone to stop ConcurrentModification.
        ActivePotionEffects.addAll(P.getActivePotionEffects());
        for (PotionEffect PE : ActivePotionEffects) {
            P.removePotionEffect(PE.getType());
        }
    }
    
    public void resetToPlay() {
        Player P = getPlayer();
        P.setFoodLevel(7);
        P.setFallDistance(0);
        P.setFireTicks(0);
        zeroExp();
        removeAllPotions();
        resetInventory();
    }
    
    public void resetInventory() {
        PlayerInventory PI = getPlayer().getInventory();
        PI.clear();
        ItemStack Air = new ItemStack(0);
        PI.setHelmet(Air);
        PI.setChestplate(Air);
        PI.setLeggings(Air);
        PI.setBoots(Air);
    }
    
    public ChatColor getTagColour() {
        return TagColour;
    }
    
    public void setTagColour(final ChatColor CC) {
        Player P = getPlayer();
        P.setPlayerListName(CC + P.getName());
        TagColour = CC;
        try {
            TAGAPIEvent.refreshPlayer(P);
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {}
    }
    
    public Object getMetaData(String Key) {
        return metadata.get(Key);
    }

    public void addMetaData(final String S, final Object O) {
        metadata.put(S, O);
    }

    public void removeMetaData(final String S) {
        metadata.remove(S);
    }
    
}