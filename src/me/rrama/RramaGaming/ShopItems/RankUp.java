package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.Rank;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RankUp extends ShopItem {
    
    public RankUp(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean isVisibleTo(final Player P) {
        Gamer G = RramaGaming.getGamer(P.getName());
        return (G.getRank().Value() < Rank.Advanced.Value());
    }
    
    @Override
    public boolean canBuy(final Player P) {
        final Gamer G = RramaGaming.getGamer(P.getName());
        final Rank R = G.getRank();
        if (R == Rank.Admin || R == Rank.Moderator) {
            P.sendMessage(ChatColor.YELLOW + "Staff don't buy ranks.");
        } else if (R == Rank.Donator) {
            P.sendMessage(ChatColor.YELLOW + "Donator is the top non-staff rank.");
        } else if (R == Rank.Advanced) {
            P.sendMessage(ChatColor.YELLOW + "You already have the highest buyable rank. Consider donating to get the next rank.");
        } else return true;
        return false;
    }
    
    @Override
    public boolean perform(final Player P) {
        final Gamer G = RramaGaming.getGamer(P.getName());
        final Rank R = G.getRank();
        boolean b = Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "manpromote " + P.getName() + " " + R.Up());
        if (!b) {
            P.sendMessage(ChatColor.RED + "Error whilst ranking you. Please tell a mod.");
            Bukkit.getLogger().warning((RramaGaming.RGTagNo + ChatColor.RED + "Unable to rank " + P.getName() + " rankup command did not send."));
        }
        return b;
    }
    
}