package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class TNTPlacing implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void TNTPlaceAndPrime(BlockPlaceEvent event) {
        if (event.getBlock().getTypeId() == 46) {
            if (event.getBlock().getWorld() == Bukkit.getWorlds().get(0)) {
                final Location l = event.getBlock().getLocation();
                event.getBlock().setTypeId(0);
                ((TNTPrimed) l.getWorld().spawnEntity(l, EntityType.PRIMED_TNT)).setFuseTicks(60);
            }
        }
    }
}