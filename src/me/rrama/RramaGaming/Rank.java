package me.rrama.RramaGaming;

import org.bukkit.ChatColor;

/**
 * 
 * Represents the ranks.
 */
public enum Rank {
    
    /**
     * Banned, The rank where player is unable to join or do anything.
     */
    Banned (ChatColor.BLACK, 1),
    
    /**
     * Guest, The default rank.
     */
    Guest (ChatColor.GRAY, 2),
    
    /**
     * Regular.
     */
    Regular (ChatColor.GREEN, 3),
    
    /**
     * Advanced.
     */
    Advanced (ChatColor.AQUA, 4),
    
    /**
     * Donator, Given to players who have donated to the server.
     */
    Donator (ChatColor.GOLD, 5),
    
    /**
     * Trusted, The first unofficial staff rank.
     */
    Trusted (ChatColor.DARK_RED, 6),
    
    /**
     * Moderator, The first official staff rank.
     */
    Moderator (ChatColor.YELLOW, 7),
    
    /**
     * Admin, Highest rank.
     */
    Admin (ChatColor.DARK_PURPLE, 8),
    
    /**
     * Unraked, this should never be used or thrown.
     */
    Unranked (ChatColor.MAGIC, 0);
    
    private final ChatColor RankColour;
    private final int Value;
    
    Rank(final ChatColor RankColour, final int Value) {
        this.RankColour = RankColour;
        this.Value = Value;
    }
    
    public ChatColor Colour() {
        return RankColour;
    }
    
    public int Value() {
        return Value;
    }
    
    public Rank Up() {
        return Rank.values()[this.Value()+1];
    }
    
    public Rank Down() {
        return Rank.values()[this.Value()-1];
    }
}
