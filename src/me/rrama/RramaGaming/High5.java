package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
     
public class High5 extends JavaPlugin {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String args[]) {
        if(commandLabel.equalsIgnoreCase("high5") && args.length == 1) {
            Player Victim = Bukkit.getPlayer(args[0]);
            if (Victim != null) {
                String SN = sender.getName();
                String VN = Victim.getName();
                Bukkit.broadcastMessage(ChatColor.GREEN + SN + " high5'd " + VN + "!");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Cannot find player spesifed.");
            }
            return true;
        } else return false;
    }
}
