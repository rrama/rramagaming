package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.HandlerList;
import org.bukkit.event.world.WorldEvent;

public class MoonPhaseChangeEvent extends WorldEvent {
    
    private static final HandlerList handlers = new HandlerList();
    protected static MoonPhase MP;
    
    public MoonPhaseChangeEvent(final World world, final MoonPhase MP) {
        super(world);
        MoonPhaseChangeEvent.MP = MP;
    }
    
    public MoonPhase getMoonPhase() {
        return MP;
    }
    
    public static MoonPhase getMoonPhaseByInt(final int I) {
        return MoonPhase.values()[I];
    }
    
    public static void MoonCheck() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(RramaGaming.ThisPlugin, new Runnable() {
            
            @Override
            public void run() {
                World worldy = Bukkit.getWorlds().get(0);
                long T = worldy.getFullTime();
                long D = T/24000;
                int days = ((int)D);
                int phaseInt = days%8;
                MoonPhase NewPhase = getMoonPhaseByInt(phaseInt);
                if (NewPhase != MP) {
                    new MoonPhaseChangeEvent(worldy, NewPhase);
                }
            }
            
        }, 0, 30*20);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public static enum MoonPhase {
        FullMoon,
        WaningGibbous,
        LastQuarter,
        WaningCrescent,
        NewMoon,
        WaxingCrescent,
        FirstQuarter,
        WaxingGibbous;
    }
}
