package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class TitleCMDs implements TabExecutor {
    
    public static ArrayList<String> HasATitleToUse = new ArrayList<>();
    public static ArrayList<String> HasATitleColourToUse = new ArrayList<>();
    public static ArrayList<String> HasANameColourToUse = new ArrayList<>();
    
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLable, String[] args) {
        ArrayList<String> outcomes = new ArrayList<>();
        if (commandLable.equalsIgnoreCase("TitleColour") || commandLable.equalsIgnoreCase("TitleColor") ||
                commandLable.equalsIgnoreCase("NameColour") || commandLable.equalsIgnoreCase("NameColor")) {
            Bukkit.dispatchCommand(sender, "/Colours");
        }
        return outcomes;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Title")) {
            if (args.length == 1) {
                if (sender instanceof Player) {
                    Player ps = (Player)sender;
                    String pn = ps.getName();
                    if (ps.hasPermission("rramagaming.title") || HasATitleToUse.contains(pn)) {
                        Gamer CP = RramaGaming.getGamer(pn);
                        CP.setTitle(args[0]);
                        if (HasATitleToUse.contains(pn)) {
                            OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their title (Had 1 to use).");
                            HasATitleToUse.remove(pn);
                        } else {
                            OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their title.");
                        }
                        Bukkit.broadcastMessage(RramaGaming.RGTagB + pn + " now has the wonderfull new title [" + args[0] + "].");
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You need to be a player to have a title.");
                }
                return true;
            } else if (args.length == 2) {
                if (sender.hasPermission("rramagaming.title")) {
                    String s1 = args[0];
                    Player victim = Bukkit.getPlayer(s1);
                    if (victim != null) {
                        String victimN = victim.getName();
                        Gamer CP = RramaGaming.getGamer(victimN);
                        CP.setTitle(args[1]);
                        String pn = sender.getName();
                        OpChat.SendMessageToAllOps("RramaGaming", pn + " changed " + victimN + "'s title.");
                        Bukkit.broadcastMessage(RramaGaming.RGTagB + victimN + " now has the wonderfull new title [" + args[1] + "].");
                        return true;
                    } else return false;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    return true;
                }
            } else return false;
            
        } else if (commandLable.equalsIgnoreCase("TitleColour") || commandLable.equalsIgnoreCase("TitleColor")) {
            if (args.length == 1) {
                if (sender instanceof Player) {
                    Player ps = (Player)sender;
                    String pn = ps.getName();
                    if (ps.hasPermission("rramagaming.titlecolour") || HasATitleColourToUse.contains(pn)) {
                        String s1 = args[0];
                        String s = s1.toUpperCase();
                        try {
                            ChatColor C = ChatColor.valueOf(s);
                            Gamer CP = RramaGaming.getGamer(pn);
                            CP.setTitleColour(C);
                            if (HasATitleColourToUse.contains(pn)) {
                                OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their title colour (Had 1 to use).");
                                HasATitleColourToUse.remove(pn);
                            } else {
                                OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their title colour.");
                            }
                            Bukkit.broadcastMessage(RramaGaming.RGTagB + pn + " now has the wonderfull new title colour " + C + s + ChatColor.BLUE + ".");
                        } catch (IllegalArgumentException ex) {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invaild colour.");
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You need to be a player to have a title colour.");
                }
                return true;
            } else if (args.length == 2) {
                if (sender.hasPermission("rramagaming.titlecolour")) {
                    String s1 = args[0];
                    Player victim = Bukkit.getPlayer(s1);
                    if (victim != null) {
                        String s2b = args[1];
                        String s2 = s2b.toUpperCase();
                        try {
                            ChatColor C = ChatColor.valueOf(s2);
                            String victimN = victim.getName();
                            Gamer CP = RramaGaming.getGamer(victimN);
                            CP.setTitleColour(C);
                            String pn = sender.getName();
                            OpChat.SendMessageToAllOps("RramaGaming", pn + " changed " + victimN + "'s title colour.");
                            Bukkit.broadcastMessage(RramaGaming.RGTagB + victimN + " now has the wonderfull new title colour " + C + s2 + ChatColor.BLUE + ".");
                        } catch (IllegalArgumentException ex) {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invaild colour.");
                        }
                        return true;
                    } else return false;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    return true;
                }
            } else return false;
            
        } else if (commandLable.equalsIgnoreCase("NameColour") || commandLable.equalsIgnoreCase("NameColor")) {
            if (args.length == 1) {
                if (sender instanceof Player) {
                    Player ps = (Player)sender;
                    String pn = ps.getName();
                    if (ps.hasPermission("rramagaming.namecolour") || HasANameColourToUse.contains(pn)) {
                        String s1 = args[0];
                        String s = s1.toUpperCase();
                        try {
                            ChatColor C = ChatColor.valueOf(s);
                            Gamer CP = RramaGaming.getGamer(pn);
                            CP.setNameColour(C);
                            if (HasANameColourToUse.contains(pn)) {
                                OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their name colour (Had 1 to use).");
                                HasANameColourToUse.remove(pn);
                            } else {
                                OpChat.SendMessageToAllOps("RramaGaming", pn + " changed their name colour.");
                            }
                            Bukkit.broadcastMessage(RramaGaming.RGTagB + pn + " now has the wonderfull new name colour " + C + s + ChatColor.BLUE + ".");
                        } catch (IllegalArgumentException ex) {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invaild colour.");
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You need to be a player to have a name colour.");
                }
                return true;
            } else if (args.length == 2) {
                if (sender.hasPermission("rramagaming.namecolour")) {
                    String s1 = args[0];
                    Player victim = Bukkit.getPlayer(s1);
                    if (victim != null) {
                        String s2b = args[1];
                        String s2 = s2b.toUpperCase();
                        try {
                            ChatColor C = ChatColor.valueOf(s2);
                            String victimN = victim.getName();
                            Gamer CP = RramaGaming.getGamer(victimN);
                            CP.setTitleColour(C);
                            String pn = sender.getName();
                            OpChat.SendMessageToAllOps("RramaGaming", pn + " changed " + victimN + "'s name colour.");
                            Bukkit.broadcastMessage(RramaGaming.RGTagB + victimN + " now has the wonderfull new name colour " + C + s2 + ChatColor.BLUE + ".");
                        } catch (IllegalArgumentException ex) {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invaild colour.");
                        }
                        return true;
                    } else return false;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are not allowed to use this command.");
                    return true;
                }
            } else return false;
            
        } else if (commandLable.equalsIgnoreCase("Colours") || commandLable.equalsIgnoreCase("Colors") ||
                commandLable.equalsIgnoreCase("Colour") || commandLable.equalsIgnoreCase("Color")) {
            sender.sendMessage(ChatColor.YELLOW + "Avalible colours:");
            ArrayList<String> Colours = new ArrayList<>();
            for (ChatColor CC : ChatColor.values()) {
                Colours.add(CC + CC.name() + ChatColor.RESET);
            }
            sender.sendMessage(ListToString.ListToString(Colours, ", "));
            return true;
            
        } else return false;
    }
    
    public static void WriteDaInfo2AFile(final Player P, final File F1, final String S) {
        String PN = P.getName();
        File F2 = new File(F1, PN + ".txt");
        if (F2.exists() == true) {
            try (FileWriter F2FW = new FileWriter(F2)) {
                F2FW.write(S);
            } catch (IOException ex) {
                Bukkit.getLogger().warning((ChatColor.BLUE + "'IOException' Failed to write to " + PN + "'s " + F1.getName() + ".")); 
            }
            
        } else {
            try {
                boolean F2C = F2.createNewFile();
                if (F2C == false) {
                    Bukkit.getLogger().warning((ChatColor.BLUE + "Failed to make a new file for " + PN + " in the " + F1.getName() + ". File that should have been made's path: " + F2.getAbsolutePath()));
                    WriteDaInfo2AFile(P, F1, S);
                } else {
                    Bukkit.getLogger().info((ChatColor.BLUE + "Made " + PN + " a " + F1.getName()));
                    WriteDaInfo2AFile(P, F1, S);
                }
            } catch (IOException ex) {
                Bukkit.getLogger().warning((ChatColor.BLUE + "'IOException' Failed to make a new file for " + PN + " in the " + F1.getName() + ". File that should have been made's path: " + F2.getAbsolutePath()));
            }
        }
    }
}