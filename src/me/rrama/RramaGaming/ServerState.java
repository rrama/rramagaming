package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public enum ServerState {
    
    Normal,
    Stopping,
    Reload,
    Crash,
    Invalid;
    
    ServerState() {}
    
    /*
     * Only valid at plugin startup/shutdown.
     */
    public static ServerState getServerState() {
        File F = new File(FolderSetup.pluginFolder, "State.txt");
        try (Scanner S = new Scanner(F)) {
            String SNL = S.nextLine();
            return ServerState.valueOf(SNL);
        } catch (FileNotFoundException ex) {
            return ServerState.Invalid;
        }
    }
    
    public static boolean setServerState(ServerState state) {
        File F = new File(FolderSetup.pluginFolder, "State.txt");
        boolean b = true;
        if (!F.exists()) {
            try {
                b = F.createNewFile();
            } catch (IOException ex) {
                b = false;
            }
        }
        
        try (FileWriter FW = new FileWriter(F)) {
            FW.write(state.name());
        } catch (IOException ex) {
            b = false;
        }
        return b;
    }
    
}