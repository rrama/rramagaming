package me.rrama.RramaGaming;

import java.util.ArrayList;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class GamingPlugin extends JavaPlugin {
    
    public boolean RoundPlugin = true, ForceStart = true, AllowAddingTime = false; //ForceStart ignored if RoundPlugin is false.
    public ArrayList<String> Maps = new ArrayList<>();
    public ArrayList<ShopItem> ShopItems = new ArrayList<>();
    public int MinimumPlayersNeeded = 0;
    public Timer timer = null;
    public String TagNo, TagB;
    
    @Override
    public void onEnable() {
        RramaGaming.DisabledGames.remove(getName());
        TagNo = ChatColor.BLACK + "[" + getName() + "] ";
        TagB = TagNo + ChatColor.BLUE;
        onGameEnable();
        RramaGaming.EnabledGames.add(getName());
    }
    
    public void onGameEnable() {}
    
    @Override
    public void onDisable() {
        RramaGaming.EnabledGames.remove(getName());
        if (timer != null) {
            timer.cancel();
        }
        onGameDisable();
        if (RramaGaming.gameInPlay.equals(getName())) {
            Bukkit.broadcastMessage(TagB +  "Round ended. Due to plugin disabling.");
            RramaGaming.gameInPlay = "null";
            for (Gamer G : RramaGaming.getGamers()) {
                if (G.m()) {
                    G.setGamePlaying("null");
                    G.setTagColour(ChatColor.WHITE);
                }
            }
            RramaGaming.R = RoundState.Off;
            RramaGaming.WasInRound.clear();
            RramaGaming.teleportAllPlayersTo(GamingMaps.GamingMaps.get("Spawn").getSpawn());
        }
        RramaGaming.DisabledGames.add(getName());
    }
    
    public void onGameDisable() {}
    
    public void callStartPreGame() {
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.m()) G.resetToPlay();
        }
        startPreGame();
    }
    
    public void startPreGame() {}
    
    public void callStartRound() {
        if (RramaGaming.MoreAvalibleGamersThan(MinimumPlayersNeeded)) {
            RramaGaming.R = RoundState.InGame;
            RramaGaming.WasInRound.clear();
            startRound();
        } else {
            StartAndStopGame.MapVote(this);
        }
    }
    
    public void startRound() {}
    
    public void callEndRound(boolean FinalStop) {
        RramaGaming.R = RoundState.AfterGame;
        RramaGaming.WasInRound.clear();
        try {
            if (timer.isRunning()) {
                timer.cancel();
            }
        } catch (NullPointerException ex) {}
        if (StartAndStopGame.Need2Cycle) {
            FinalStop = true;
        }
        endRound(FinalStop);
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.m()) {
                G.setTagColour(ChatColor.WHITE);
                G.resetToPlay();
            }
        }
        RramaGaming.MapInPlay = GamingMaps.GamingMaps.get("Spawn");
        if (FinalStop) {
            Bukkit.broadcastMessage(TagB +  getName() + " has stopped.");
            RramaGaming.R = RoundState.Off;
            for (Gamer G : RramaGaming.getGamers()) {
                if (G.m()) G.setGamePlaying("null");
            }
            RramaGaming.gameInPlay = "null";
            RramaGaming.teleportAllPlayersTo(GamingMaps.GamingMaps.get("Spawn").getSpawn());
            if (!StartAndStopGame.Need2Cycle) return;
        }
        StartAndStopGame.MapVote(this);
    }
    
    public void endRound(boolean FinalStop) {}
}