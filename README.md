# README #

Please note this repository's **code has not been updated since early 2013!**

## Content ##

In this repository is an API for easily coding mini-games into the game Minecraft.
This includes features such as;

* In-game commands.
* Handling the states of a game.
* Handling ingame events (e.g. disabling PvP pregame and endgame).
* Loading random maps to play.
* A simple HUD for players with a modded client.
* A shop with purchasable items, currency is held via SQL.
* A rank system.
* Achievements.
* And more!

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).