package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

public class OfflineGamer {
    
    final String PN;
    String Title = null;
    ChatColor TitleColour = ChatColor.GRAY;
    ChatColor NameColour = ChatColor.GRAY;
    private int ICookies;
    
    OfflineGamer(final OfflinePlayer OP) {
        PN = OP.getName();
        updateTitle();
        updateTitleColour();
        updateNameColour();
    }
    
    public String getName() {
        return PN;
    }
    
    public String getTitle() {
        return Title;
    }

    public void setTitle(final String Title) {
        TitleCMDs.WriteDaInfo2AFile(Bukkit.getPlayer(PN), FolderSetup.TitlesFolder, Title);
        updateTitle();
    }

    public final void updateTitle() {
        File TF = new File(FolderSetup.TitlesFolder, PN + ".txt");
        if (TF.exists() == true) {
            try {
                Scanner s = new Scanner(TF);
                s.reset();
                String next = s.next();
                Title = next;
            } catch (FileNotFoundException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + "'s Title file could not be found even though it exists."));
            }
        }
    }

    public ChatColor getTitleColour() {
        return TitleColour;
    }

    public void setTitleColour(final ChatColor TitleColour) {
        TitleCMDs.WriteDaInfo2AFile(Bukkit.getPlayer(PN), FolderSetup.TitleColoursFolder, TitleColour.name());
        updateTitleColour();
    }

    public final void updateTitleColour() {
        File TCF = new File(FolderSetup.TitleColoursFolder, PN + ".txt");
        if (TCF.exists() == true) {
            try {
                Scanner s = new Scanner(TCF);
                s.reset();
                String next = s.next();
                if (ChatColor.valueOf(next) != null) {
                    ChatColor C = ChatColor.valueOf(next);
                    TitleColour = C;
                } else {
                    Bukkit.getPlayer(PN).sendMessage(ChatColor.GOLD + "Your Title colour file is corrupt.");
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + " has a corrupt Title colour file."));
                }
            } catch (FileNotFoundException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + "'s Title colour file could not be found even though it exists."));
            }
        } else {
            TitleColour = getNameColour();
        }
    }

    public ChatColor getNameColour() {
        return NameColour;
    }

    public void setNameColour(final ChatColor NameColour) {
        TitleCMDs.WriteDaInfo2AFile(Bukkit.getPlayer(PN), FolderSetup.PlayerColoursFolder, NameColour.name());
        updateNameColour();
    }

    public final void updateNameColour() {
        File PCF = new File(FolderSetup.PlayerColoursFolder, PN + ".txt");
        if (PCF.exists() == true) {
            try {
                Scanner s = new Scanner(PCF);
                s.reset();
                String next = s.next();
                if (ChatColor.valueOf(next) != null) {
                    ChatColor C = ChatColor.valueOf(next);
                    NameColour = C;
                } else {
                    Bukkit.getPlayer(PN).sendMessage(ChatColor.GOLD + "Your Name colour file is corrupt.");
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + " has a corrupt Name colour file."));
                }
            } catch (FileNotFoundException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + "'s Name colour file could not be found even though it exists."));
            }
        }
    }
    
    public int getCookies() {
        return ICookies;
    }

    public void addCookies(final int NewCookies) throws CookieFileException {
        setCookies(getCookies() + NewCookies);
    }

    public void setCookies(final int NewCookies) throws CookieFileException {
        File PCF = new File(FolderSetup.PlayersCookiesFolder, PN + ".txt");
        if (PCF.exists() == true) {
            try (FileWriter FWPCF = new FileWriter(PCF)){
                String s = String.valueOf(NewCookies);
                FWPCF.write(s);
                FWPCF.close();
                RGAchievements.AwardAchievement(PN, RGAchievements.Achievement.Earner);
                RramaGaming.getGamer(PN).updateCookies();
            } catch (IOException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not add Cookies to " + PN + "'s Cookie file."));
                throw new CookieFileException(CookieFileException.CookieFileProblem.IsUnwritable, PN);
            }
        } else {
            throw new CookieFileException(CookieFileException.CookieFileProblem.DoesNotExist, PN);
        }
        updateCookies();
    }

    public void updateCookies() throws CookieFileException {
        File PCF = new File(FolderSetup.PlayersCookiesFolder, PN + ".txt");
        if (PCF.exists()) {
            try {
                Scanner s = new Scanner(PCF);
                s.reset();
                String next = s.nextLine();
                if (next.matches("-?\\d+(.\\d+)?") == true) {
                    ICookies = Integer.parseInt(next);
                } else {
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + PN + " has a corrupt Cookie File. (updateCookies())"));
                    ICookies = -1337;
                    throw new CookieFileException(CookieFileException.CookieFileProblem.IsCorrupt, PN);
                }
            } catch (FileNotFoundException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not find Cookie file for " + PN + " even though it exists. (updateCookies())"));
                ICookies = -1337;
                throw new CookieFileException(CookieFileException.CookieFileProblem.WasNotFound, PN);
            } catch (NoSuchElementException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "'NoSuchElementException' for " + PN + "'s Cookie file. (updateCookies())"));
                ICookies = -1337;
                throw new CookieFileException(CookieFileException.CookieFileProblem.NoSuchElement, PN);
            }
        } else {
            ICookies = -1337;
            throw new CookieFileException(CookieFileException.CookieFileProblem.DoesNotExist, PN);
        }
    }
    
}
