package me.rrama.RramaGaming;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;

public class MaxHeight implements Listener {
    
    public static int BuildHeight = -1337;
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent event) {
        CheckMaxHeight(event, event.getBlock(), event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent event) {
        CheckMaxHeight(event, event.getBlock(), event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        CheckMaxHeight(event, event.getBlockClicked(), event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        CheckMaxHeight(event, event.getBlockClicked(), event.getPlayer());
    }
    
    public static void CheckMaxHeight(final Cancellable C, final Block B, final Player P) {
        if (Ref.Refs.contains(P.getName())) return;
        if (B.getY() >= BuildHeight) {
            if (BuildHeight != -1337) {
                P.sendMessage(ChatColor.RED + "Max build height.");
            }
            C.setCancelled(true);
        } else if (RramaGaming.gameInPlay.equals("null") || !(RramaGaming.R == RoundState.InGame || RramaGaming.R == RoundState.PreGame)) {
            C.setCancelled(true);
        }
    }
}
