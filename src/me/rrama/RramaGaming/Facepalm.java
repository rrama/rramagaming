package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Facepalm implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Facepalm")) {
            if (args.length == 0) {
                String SN = sender.getName();
                Player p = (Player) sender;
                Bukkit.broadcastMessage(ChatColor.YELLOW + "[Facepalm]: " + ChatColor.GOLD + SN + " facepalmed!");
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("X2")) {
                    String SN = sender.getName();
                    Bukkit.broadcastMessage(ChatColor.YELLOW + "[Facepalm]: " + ChatColor.GOLD + SN + " facepalm X2 COMBO'd!");
                }
                return true;
            } else return false;
        } else return false;
    }
}
