package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitTask;

public class Timer {
    
    private final GamingPlugin Owner;
    private final String OwnerN;
    private final boolean Start;
    private final String StartStopText;
    private boolean Running = true;
    private int Time;
    private int Time2AddI = 0;
    private boolean Time2AddB = false; //Quicker to check. Also reduces ConcurrentModificationExceptions slightly.
    private BukkitTask TimeTask;
    
    public Timer(GamingPlugin GP, boolean start, int time) {
        Owner = GP;
        OwnerN = Owner.getName();
        Start = start;
        if (Start) {
            RramaGaming.R = RoundState.PreGame;
            StartStopText = "Round starts in: ";
        } else {
            StartStopText = "Round ends in: ";
        }
        Time = time;
        if (!RramaGaming.gameInPlay.equals(OwnerN)) {
            Bukkit.getLogger().warning((RramaGaming.RGTagB + OwnerN + " called a timer when it is not in play."));
            return;
        }
        TimerMain();
        Owner.timer = this;
    }
    
    private void TimerMain() {
        TimeTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Owner, new Runnable() {

            @Override
            public void run() {
                if (Time > 0) {
                    if (RramaGaming.gameInPlay.equals(OwnerN)) {
                        if (Time2AddB) {
                            Time += Time2AddI;
                            Time2AddI = 0;
                        }
                        if (Time % 120 == 0) {
                            Bukkit.broadcastMessage(Owner.TagB + StartStopText + ChatColor.RED + Time/60 + " minutes.");
                        } else if (Time == 60 || Time == 30 || Time == 10 || Time == 5 || Time == 4 || Time == 3 || Time == 2 || Time == 1) {
                            Bukkit.broadcastMessage(Owner.TagB + StartStopText + ChatColor.RED + Time + " seconds.");
                        }
                        --Time; //I believe this is the most effcient method.
                    } else {
                        Bukkit.getLogger().warning((Owner.TagB + "Time() was running when " + OwnerN + " was not in play."));
                        cancel();
                    }
                } else if (Time == 0) {
                    cancel();
                    if (Start) {
                        Owner.callStartRound();
                    } else {
                        Owner.callEndRound(false);
                    }
                } else {
                    Bukkit.getLogger().warning((Owner.TagB + "Timer's time went below 0."));
                }
            }
        }, 20, 20);
    }
    
    public int getTime() {
        return Time;
    }
    
    public boolean addTime(int time) {
        if (getTime() < 5) return false;
        Time2AddI = time;
        Time2AddB = true;
        return true;
    }
    
    public boolean isRunning() {
        return Running;
    }
    
    public void cancel() {
        Running = false;
        TimeTask.cancel();
    }
}