package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

public class FunVote implements TabExecutor {
    
    public static int FunVoteInt = 0; //0 = Not in, 1 = Being asked, 2 = Results.
    public static HashMap<String, Integer> VoteOptionsWithCount = new HashMap<>();
    public static ArrayList<String> VoteOptions = new ArrayList<>();
    public static ArrayList<CommandSender> HasVoted = new ArrayList<>();
    
    
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLable, String[] args) {
        ArrayList<String> outcomes = new ArrayList<>();
        if (commandLable.equalsIgnoreCase("FunVote") && args.length == 1) {
            if (FunVoteInt == 1) {
                outcomes.addAll(VoteOptions);
            } else {
                sender.sendMessage(ChatColor.YELLOW + "No fun vote currently cast.");
            }
        }
        return outcomes;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {

        if (commandLable.equalsIgnoreCase("FunVote")) {
            if (FunVoteInt == 0) {
                sender.sendMessage(ChatColor.YELLOW + "A fun vote has not been cast.");
                return true;
            } else if (FunVoteInt == 2) {
                sender.sendMessage(ChatColor.YELLOW + "You have just missed the fun vote.");
                return true;
            } else {
                if (HasVoted.contains(sender)) {
                    sender.sendMessage(ChatColor.YELLOW + "You have already voted.");
                    return true; 
                } else {
                    
                    if (args.length == 1) {
                        String s = args[0];
                        if (VoteOptionsWithCount.containsKey(s)) {
                            final int i = VoteOptionsWithCount.get(s);
                            final int i2 = i + 1;
                            VoteOptionsWithCount.put(s, i2);
                            sender.sendMessage(ChatColor.YELLOW + "Thank you for voting.");
                            HasVoted.add(sender);
                        } else {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invalid option.");
                        }
                        return true;
                    } else if (args.length == 2) {
                        if (args[0].equalsIgnoreCase("Option") && args[1].matches("-?\\d+(.\\d+)?")) {
                            final int TheirOption = Integer.parseInt(args[1]);
                            final int AmountOfOptions = VoteOptions.size() + 1;
                            if (TheirOption > 0 && TheirOption <= AmountOfOptions) {
                                final String s = VoteOptions.get(TheirOption - 1);
                                final int i = VoteOptionsWithCount.get(s);
                                final int i2 = i + 1;
                                VoteOptionsWithCount.put(s, i2);
                                sender.sendMessage(ChatColor.YELLOW + "Thank you for voting.");
                                HasVoted.add(sender);
                            } else {
                                sender.sendMessage(ChatColor.YELLOW + "That option is out of bounds of the Options size. (Has to be greagter than 0)");
                            }
                            return true;
                        } else return false;
                    } else return false;
                    
                }
            }
            
        } else if (commandLable.equalsIgnoreCase("FunVoteCast")) {
            if (FunVoteInt == 0) {
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.YELLOW + "Too short. Needs more arguments.");
                    return true;
                }
                String s = args[0];
                if (s.matches("-?\\d+(.\\d+)?") == true) {
                    int i = Integer.parseInt(s);
                    if (i <= 0) {
                        sender.sendMessage(ChatColor.YELLOW + "Number of options cannot be 0 or less.");
                    } else if (i > 9) {
                        sender.sendMessage(ChatColor.YELLOW + "Number of options cannot be more than 9.");
                    } else {
                        FunVoteInt = 1;
                        VoteBroadcast(args, i);
                    }
                    return true;
                } else return false;
            } else {
                sender.sendMessage(ChatColor.YELLOW + "A fun vote is already being cast.");
                return true;
            }
            
        } else return false;
    }
    
    public static void VoteBroadcast(final String[] args, final int i) {
        VoteOptionsWithCount.clear();
        VoteOptions.clear();
        HasVoted.clear();
        String VoteQuestion = "";
        int i2 = 0;
        for (String s : args) {
            if (i2 == 0) {
                i2++;
            } else if (i2 <= i) {
                VoteOptionsWithCount.put(s, 0);
                VoteOptions.add(s);
                i2++;
            } else {
                VoteQuestion = VoteQuestion + s + " ";
            }
        }
        final String VoteQuestion2 = VoteQuestion;
        Bukkit.broadcastMessage(ChatColor.GREEN + "VOTE: " + ChatColor.AQUA + VoteQuestion2 + ChatColor.GREEN + "?");
        int i3 = 1;
        for (String s : VoteOptionsWithCount.keySet()) {
            Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "" + i3 + ". " + ChatColor.RED + s);
            i3++;
        }
        Bukkit.broadcastMessage(ChatColor.BLUE + "Vote with /FunVote [Option]");
        
        Bukkit.getScheduler().runTaskLaterAsynchronously(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                FunVoteInt = 2;
                Bukkit.broadcastMessage(ChatColor.GREEN + "RESULTS: for " + ChatColor.AQUA + VoteQuestion2 + ChatColor.GREEN + "?");
                for (String s : VoteOptionsWithCount.keySet()) {
                    Bukkit.broadcastMessage(ChatColor.RED + s + ChatColor.GREEN + " got " + ChatColor.LIGHT_PURPLE + VoteOptionsWithCount.get(s) + ChatColor.GREEN + " votes.");
                }
                FunVoteInt = 0;
            }
        }, 30*60);
    }
}