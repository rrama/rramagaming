package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import me.rrama.RramaGaming.ShopItems.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Shop implements CommandExecutor {
    
    public static ArrayList<ShopItem> GamingShopItems = new ArrayList<>();
    public static ChatColor CRed = ChatColor.RED;
    public static ChatColor CGreen = ChatColor.GREEN;
    
    public static void ItemPricesReg(final File FileItemPrices) {
        try {
            Scanner s = new Scanner(FileItemPrices);
            s.reset();
            int count = 1;
            while (s.hasNextLine()) {
                String S = s.nextLine();
                String[] Sy = S.split(": ");
                if (Sy.length != 2) {
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + "The line '" + S + "' does not follow the correct format for 'ItemPrices.txt'."));
                } else if (!Sy[1].matches("-?\\d+(.\\d+)?")) {
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + "'" + Sy[1] + "' in '" + S + "' in 'ItemPrices.txt' is not an integer."));
                } else {
                    String Name = Sy[0];
                    int ItemPrice = Integer.parseInt(Sy[1]);
                    //IndiItems reg
                    if (count == 1) {
                        new RankUp(null, Name, ItemPrice);
                    } else if (count == 2) {
                        new Title(null, Name, ItemPrice);
                    } else if (count == 3) {
                        new TitleColour(null, Name, ItemPrice);
                    } else if (count == 4) {
                        new NameColour(null, Name, ItemPrice);
                    }
                    //IndiItems reg end
                }
                count++;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not find File 'ItemPrices.txt' even though it exists."));
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Shop") || commandLable.equalsIgnoreCase("Store") || commandLable.equalsIgnoreCase("Buy")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Console can not use /shop");
                return true;
            }
            Player P = (Player)sender;
            String PN = P.getName();
            Gamer G = RramaGaming.getGamer(PN);
            RGAchievements.AwardAchievement(PN, Achievement.Just_Browsing);
            if (G.isRef()) {
                sender.sendMessage(ChatColor.YELLOW + "Refs don't need no shop.");
                return true;
            }
            if (args.length == 0) {
                sender.sendMessage(ChatColor.YELLOW + "---- Shop ----");
                sender.sendMessage(ChatColor.YELLOW + "To use the shop type /shop [Item number]");
                int count = 1;
                for (ShopItem Item : GamingShopItems) {
                    if (Item.isVisibleTo(P)) {
                        sender.sendMessage(CRed + "" + count + ". " + CGreen + "" + Item.getName() + ChatColor.GOLD + " " + Item.getPrice() + " Cookies");
                    }
                    count++;
                }
                if (!"null".equals(RramaGaming.gameInPlay) && G.m()) {
                    GamingPlugin GIP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                    for (ShopItem Item : GIP.ShopItems) {
                        if (Item.isVisibleTo(P)) {
                            sender.sendMessage(CRed + "" + count + ". " + CGreen + "" + Item.getName() + ChatColor.GOLD + " " + Item.getPrice() + " Cookies");
                        }
                        count++;
                    }
                }
                sender.sendMessage(ChatColor.YELLOW + "Use /shop page [number] for a more organised view.");
            } else if(args.length == 1 && args[0].matches("-?\\d+(.\\d+)?")) {
                int item = Integer.parseInt(args[0]);
                if (item > 0) {
                    int AmountOfCookies = RramaGaming.getGamer(PN).getCookies();
                    RGAchievements.AwardAchievement(PN, Achievement.Buyer);
                    if (item < 5) {
                        GiveItem(GamingShopItems.get(item-1), P, AmountOfCookies);
                    } else if (!"null".equals(RramaGaming.gameInPlay) && G.m()) {
                        GamingPlugin GIP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                        try {
                            GiveItem(GIP.ShopItems.get(item-5), P, AmountOfCookies);
                        } catch (IndexOutOfBoundsException ex) {
                            sender.sendMessage(ChatColor.YELLOW + "That is an invalid shop item.");
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "That is an invalid shop item.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Item number must be greater than 0.");
                }
            } else if (args.length == 2 && "page".equalsIgnoreCase(args[0])) {
                if (args[1].matches("-?\\d+(.\\d+)?")) {
                    int page = Integer.parseInt(args[1]);
                    if (page > 0) {
                        if (page == 1) {
                            sender.sendMessage(ChatColor.YELLOW + "---- Shop ----");
                            sender.sendMessage(ChatColor.YELLOW + "To use the shop type /shop [Item number]");
                            sender.sendMessage(ChatColor.YELLOW + "Page 1:");
                            int count = 1;
                            for (ShopItem Item : GamingShopItems) {
                                if (Item.isVisibleTo(P)) {
                                    sender.sendMessage(CRed + "" + count + ". " + CGreen + "" + Item.getName() + ChatColor.GOLD + " " + Item.getPrice() + " Cookies");
                                }
                                count++;
                            }
                        } else if (!"null".equals(RramaGaming.gameInPlay) && G.m()) {
                            GamingPlugin GIP = (GamingPlugin)Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
                            try {
                                GIP.ShopItems.get(((page-1)*4)+1);
                            } catch (IndexOutOfBoundsException ex) {
                                sender.sendMessage(ChatColor.YELLOW + "Invalid shop page number. Try a lower number");
                                return true;
                            }
                            sender.sendMessage(ChatColor.YELLOW + "---- Shop ----");
                            sender.sendMessage(ChatColor.YELLOW + "To use the shop type /shop [Item number]");
                            sender.sendMessage(ChatColor.YELLOW + "Page " + page + ":");
                            for (int count = ((page-1)*4)+1; count < ((page-1)*4)+5; ++count) {
                                try {
                                    ShopItem Item = GIP.ShopItems.get(count-4);
                                    if (Item.isVisibleTo(P)) {
                                        sender.sendMessage(CRed + "" + count + ". " + CGreen + "" + Item.getName() + ChatColor.GOLD + " " + Item.getPrice() + " Cookies");
                                    }
                                } catch (IndexOutOfBoundsException ex) {
                                    break;
                                }
                            }
                        } else {
                            sender.sendMessage(ChatColor.YELLOW + "Invalid shop page number. Try a lower number");
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "Invalid shop page number. Page number must be greater than 0 and an intiger.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Invalid shop page.");
                }
            } else return false;
            return true;
        } else return false;
    }
    
    public static void GiveItem(final ShopItem Item, final Player P, final int AmountOfCookiesI) {
        if (AmountOfCookiesI < Item.getPrice()) {
            P.sendMessage(ChatColor.YELLOW + "You do not have enough Cookies for " + Item.getName() + ".");
            return;
        }
        if (Item.canBuy(P)) {
            Gamer G = RramaGaming.getGamer(P.getName());
            try {
                G.addCookies(-Item.getPrice());
                if (Item.perform(P)) {
                    P.sendMessage(ChatColor.YELLOW + "You brought '" + Item.getName() + "'.");
                } else {
                    P.sendMessage(ChatColor.YELLOW + "Error whilst buying '" + Item.getName() + "', you will be refunded now.");
                    G.addCookies(Item.getPrice());
                }
            } catch (CookieFileException ex) {
                ex.sendConsoleMessage();
                ex.sendPlayerMessage();
            }
        }
    }
}