package me.rrama.RramaGaming;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.rrama.RramaGaming.CookieFileException.CookieFileProblem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cookies implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Cookies")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    try {
                        Player P =(Player)sender;
                        String PN = P.getName();
                        int cookies = getPlayersCookies(PN);
                        sender.sendMessage(ChatColor.YELLOW + "You have " + cookies + " Cookies.");
                    } catch (CookieFileException ex) {
                        CatchCookieFileException(sender, ex);
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are a Console... Consoles do not get Cookies.");
                }
                return true;
            
            } else if (args.length == 1) {
                try {
                    OfflinePlayer OP = Bukkit.getOfflinePlayer(args[0]);
                    int cookies = getPlayersCookies(OP.getName());
                    sender.sendMessage(ChatColor.YELLOW + OP.getName() + " has " + cookies + " Cookies.");
                } catch (CookieFileException ex) {
                    CatchCookieFileException(sender, ex);
                }
                return true;
            } else return false;
        
        } else if (commandLable.equalsIgnoreCase("SetCookies")) {
            if (args.length == 1) {
                if (sender instanceof Player) {
                    if (args[0].matches("-?\\d+(.\\d+)?") == true) {
                        try {
                            int i = Integer.parseInt(args[0]);
                            Player P =(Player)sender;
                            String PN = P.getName();
                            setCookies(PN, i);
                            Bukkit.dispatchCommand(sender, "Cookies");
                        } catch (CookieFileException ex) {
                            ex.sendPlayerMessage();
                            ex.sendConsoleMessage();
                        }
                        return true;
                    } else return false;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are a Console... Consoles do not get Cookies.");
                    return true;
                }
            
            } else if (args.length == 2) {
                if (args[1].matches("-?\\d+(.\\d+)?") == true) {
                    try {
                        int i = Integer.parseInt(args[1]);
                        String PNA = args[0];
                        if (Bukkit.getPlayerExact(PNA) != null) {
                            PNA = Bukkit.getPlayerExact(PNA).getName();
                        }
                        setCookies(PNA, i);
                        Bukkit.dispatchCommand(sender, "Cookies " + PNA);
                        Player P = Bukkit.getPlayerExact(PNA);
                        if (P != null) {
                            P.sendMessage(ChatColor.GOLD + "You now have " + i + " Cookies.");
                        }
                    } catch (CookieFileException ex) {
                        CatchCookieFileException(sender, ex);
                    }
                    return true;
                } else return false;
            } else return false;
            
        } else if (commandLable.equalsIgnoreCase("AddCookies")) {
            if (args.length == 1) {
                if (sender instanceof Player) {
                    if (args[0].matches("-?\\d+(.\\d+)?") == true) {
                        try {
                            int i = Integer.parseInt(args[0]);
                            Player P =(Player)sender;
                            String PN = P.getName();
                            addCookies(PN, i);
                            Bukkit.dispatchCommand(sender, "Cookies");
                        } catch (CookieFileException ex) {
                            ex.sendPlayerMessage();
                            ex.sendConsoleMessage();
                        }
                        return true;
                    } else return false;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You are a Console... Consoles do not get Cookies.");
                    return true;
                }
            
            } else if (args.length == 2) {
                if (args[1].matches("-?\\d+(.\\d+)?") == true) {
                    try {
                        int i = Integer.parseInt(args[1]);
                        String PNA = args[0];
                        if (Bukkit.getPlayerExact(PNA) != null) {
                            PNA = Bukkit.getPlayerExact(PNA).getName();
                        }
                        addCookies(PNA, i);
                        Bukkit.dispatchCommand(sender, "Cookies " + PNA);
                        Player P = Bukkit.getPlayerExact(PNA);
                        if (P != null) {
                            P.sendMessage(ChatColor.GOLD + "You had " + i + " Cookies added.");
                        }
                    } catch (CookieFileException ex) {
                        CatchCookieFileException(sender, ex);
                    }
                    return true;
                } else return false;
            } else return false;
        } else if (commandLable.equalsIgnoreCase("SendCookies") && args.length == 2) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.YELLOW + "You are a Console... Consoles do not get Cookies.");
                return true;
            } else if (args[1].matches("-?\\d+(.\\d+)?")) {
                String P2S2 = args[0];
                if (Bukkit.getPlayerExact(P2S2) != null) {
                    P2S2 = Bukkit.getPlayerExact(P2S2).getName();
                }
                File RCF = new File(FolderSetup.PlayersCookiesFolder, P2S2 + ".txt");
                if (RCF.exists()) {
                    try {
                        int SC = getPlayersCookies(sender.getName());
                        int SC2S = Integer.parseInt(args[1]);
                        if (SC < SC2S) {
                            sender.sendMessage(ChatColor.YELLOW + "You can not send that many cookies as you have insufficient funds.");
                        } else {
                            try {
                                addCookies(sender.getName(), -SC2S);
                                try {
                                    addCookies(args[0], SC2S);
                                } catch (CookieFileException ex) {
                                    sender.sendMessage(ChatColor.YELLOW + "Failed to send cookies :( Failed at stage 'Giving them the Cookies, you will be refunded'.");
                                    try {
                                        addCookies(sender.getName(), SC2S);
                                        sender.sendMessage(ChatColor.YELLOW + "You sucessfully sent " + SC2S + " Cookies to " + P2S2 + ".");
                                        Player P = Bukkit.getPlayerExact(args[0]);
                                        if (P != null) {
                                            P.sendMessage(ChatColor.GOLD + sender.getName() + " sent you " + SC2S + " Cookies.");
                                        }
                                    } catch (CookieFileException e) {
                                        sender.sendMessage(ChatColor.YELLOW + "Failed to send cookies :( Failed at stage 'Refund, please report this.'.");
                                    }
                                }
                            } catch (CookieFileException ex) {
                                sender.sendMessage(ChatColor.YELLOW + "Failed to send cookies :( Failed at stage 'Taking the Cookies from you'.");
                            }
                        }
                    } catch (CookieFileException ex) {
                        sender.sendMessage(ChatColor.YELLOW + "Failed to send cookies :( Failed at stage 'Getting your cookies'.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Could not find a Cookie file for " + P2S2 + ".");
                }
                return true;
            } else return false;
        } else return false;
    }
    
    public static void CatchCookieFileException(CommandSender sender, CookieFileException ex) {
        OfflinePlayer p = ex.getOfflinePlayer();
        CookieFileProblem CFP = ex.getProblem();
        if (CFP.equals(CookieFileProblem.DoesNotExist)) {
            sender.sendMessage(ChatColor.YELLOW + p.getName() + " is not a player or doesn't have a Cookie file.");
        } else {
            sender.sendMessage(ChatColor.YELLOW + "Error with " + p.getName() + "'s Cookie file.");
        }
        if (p.isOnline()) {
            ex.sendPlayerMessage();
            ex.sendConsoleMessage();
        }
    }
    
    public static int getPlayersCookies(String PN) throws CookieFileException {
        OfflinePlayer OP = Bukkit.getOfflinePlayer(PN);
        if (OP != null) {
            OfflineGamer OG = new OfflineGamer(OP);
            OG.updateCookies();
            return OG.getCookies();
        } else return -1337;
    }
    
    public static void addCookies(String PN, int i) throws CookieFileException {
        int Cookies = getPlayersCookies(PN);
        int NCookies = Cookies + i;
        setCookies(PN, NCookies);
    }
    
    public static void setCookies(final String PN, final int i) throws CookieFileException {
        OfflinePlayer OP = Bukkit.getOfflinePlayer(PN);
        if (OP != null) {
            OfflineGamer OG = new OfflineGamer(OP);
            OG.setCookies(i);
        }
    }
}