package me.rrama.RramaGaming;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.gui.ArmorBar;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.HungerBar;
import org.getspout.spoutapi.gui.InGameHUD;
import org.getspout.spoutapi.gui.WidgetAnchor;
import org.getspout.spoutapi.player.SpoutPlayer;

public class SpoutGUIDanger implements Listener {
    
    public static HashMap<String, GenericLabel> CookieLabels = new HashMap<>();
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerGameModeChange(final PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode() == GameMode.SURVIVAL) {
            Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() {

                @Override
                public void run() {
                    final SpoutPlayer SP = SpoutManager.getPlayer(event.getPlayer());
                    if (SP != null) {
                        AdjustScreen(SP);
                    }
                }
            }, 10);
        }
    }
    
    public void Cache() {
//        final Plugin P = RramaGaming.ThisPlugin;
//        SpoutManager.getFileManager().addToPreLoginCache(P, new File(P.getDataFolder(), "Deg.png"));
    }
        
    public static void unGUI() throws ClassNotFoundException {
        for (GenericLabel GL : CookieLabels.values()) {
            GL.setVisible(false);
        }
    }
    
    public static void Call(final Player P) throws ClassNotFoundException {
        final SpoutPlayer SP = SpoutManager.getPlayer(P);
        AdjustScreen(SP);
//        WelcomeScreen(SP);
    }
    
    public static void removeCookieLabel(final String PN) throws ClassNotFoundException {
        CookieLabels.remove(PN);
    }
    
    public static void AdjustScreen(final SpoutPlayer SP) {
        final InGameHUD MS = SP.getMainScreen();
        final HungerBar HB = MS.getHungerBar();
        
        final ArmorBar AB = MS.getArmorBar();
        AB.setAnchor(WidgetAnchor.TOP_LEFT);
        AB.setX(295).setY(201);
        
        HB.setVisible(false);
    }
    
    public static void WelcomeScreen(final SpoutPlayer SP) {
        final GenericPopup welcome = new GenericPopup();
//        final GenericTexture logo = new GenericTexture();
        final GenericLabel esc = new GenericLabel("Press ESC to close..");
        final GenericLabel welcomelabel = new GenericLabel("Welcome to the Degnebolig's Gaming Server.");
        welcome.setAnchor(WidgetAnchor.CENTER_CENTER);
        welcomelabel.setX(165).setY(90).setWidth(100).setHeight(30);
        esc.setY(10);
//        logo.setX(180).setY(20).setWidth(64).setHeight(64);
        welcome.attachWidget(RramaGaming.ThisPlugin, welcomelabel);
//        welcome.attachWidget(RramaGaming.ThisPlugin, logo);
        welcome.attachWidget(RramaGaming.ThisPlugin, esc);
        welcome.setVisible(true);
    }
        
    public static void UpdateCookieCorner(final Gamer G) throws ClassNotFoundException {
        final SpoutPlayer SP = SpoutManager.getPlayer(G.getPlayer());
        final InGameHUD MS = SP.getMainScreen();
        GenericLabel GL;
        if (CookieLabels.containsKey(G.getName())) {
            GL = CookieLabels.get(G.getName());
            GL.setText(ChatColor.YELLOW + "Cookies: " + G.getCookies());
        } else {
            GL = new GenericLabel();
            GL.setText(ChatColor.YELLOW + "Cookies: " + G.getCookies());
            GL.setX(3).setY(3).setHeight(50).setWidth(100);
            CookieLabels.put(G.getName(), GL);
            MS.attachWidget(RramaGaming.ThisPlugin, GL);
        }
    }
}