package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class CommandAsConsole extends Handler implements CommandExecutor, Listener {
    
    public static ArrayList<String> IgnoreFromSending = new ArrayList<>();
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("CommandAsConsole") || commandLable.equalsIgnoreCase("CAC")) {
            if (Bukkit.dispatchCommand(Bukkit.getConsoleSender(), ListToString.ListToString(args, " "))) {
                sender.sendMessage(ChatColor.YELLOW + "Sucessfully dispatched that command as console.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Error dispatching that command as console.");
            }
            return true;
            
        } else if (commandLable.equalsIgnoreCase("ViewConsole")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.YELLOW + "You're using the console... Are you dumb?");
                return true;
            }
            Gamer G = RramaGaming.getGamer(sender.getName());
            if (G.isViewingConsole()) {
                G.setViewingConsole(false);
                sender.sendMessage(ChatColor.YELLOW + "You are no longer going to recieve console logs.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You will now be able to see the console logs.");
                G.setViewingConsole(true);
            }
            return true;
            
        } else return false;
    }

    @Override
    public void publish(LogRecord record) {
        String rawmsg = record.getMessage();
        if (IgnoreFromSending.contains(rawmsg)) {
            IgnoreFromSending.remove(rawmsg);
            return;
        }
        String msg = ChatColor.AQUA + "[Console] " + ChatColor.WHITE + rawmsg;
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.isViewingConsole()) {
                G.getPlayer().sendMessage(msg);
            }
        }
    }
    
    @Override
    public String toString() {
        return ("CommandAsConsole Logger");
    }
    
    public static String getConsoleViewers() {
        ArrayList<String> Viewers = new ArrayList<>();
        for (Gamer G : RramaGaming.getGamers()) {
            if (G.isViewingConsole()) Viewers.add(G.getName());
        }
        return ListToString.ListToString(Viewers, ", ");
    }
    
    @Override
    public void flush() {}
    @Override
    public void close() throws SecurityException {}
    
    //Ignored messages from events.
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent event) {
        addIgnore(event.getDeathMessage());
    }
    
    private void addIgnore(Cancellable C, String msg) {
        if (C.isCancelled()) return;
        addIgnore(msg);
    }
    private void addIgnore(String msg) {
        IgnoreFromSending.add(msg);
    }
}