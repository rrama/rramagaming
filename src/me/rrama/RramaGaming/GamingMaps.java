package me.rrama.RramaGaming;

import com.sk89q.worldedit.Vector;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Scanner;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class GamingMaps {
    
    public static HashMap<String, GamingMap> GamingMaps = new HashMap<>();
    
    public static void ReadGamingMapInfo(final File F) {
        try (Scanner S = new Scanner(F)) {
            int i = 1;
            while (S.hasNextLine()) {
                try {
                    String LS = S.nextLine();
                    if (!LS.startsWith("#")) {
                        ++i;
                        final String[] LSA = LS.split(" \\| ");
                        String[] LLSA = LSA[1].split(", ");
                        Location Spawn = new Location(Bukkit.getWorlds().get(0), Double.valueOf(LLSA[0]), Double.valueOf(LLSA[1]), Double.valueOf(LLSA[2]), Float.valueOf(LLSA[3]), Float.valueOf(LLSA[4]));

                        String[] ICenterMaps = LSA[2].split(", ");
                        int CenterMapX = Integer.parseInt(ICenterMaps[0]);
                        int CenterMapZ = Integer.parseInt(ICenterMaps[1]);

                        String[] VLSA = LSA[3].split(", ");
                        Vector ResetPoint = new Vector(Integer.valueOf(VLSA[0]), Integer.valueOf(VLSA[1]), Integer.valueOf(VLSA[2]));
                        GamingMaps.put(LSA[0], new GamingMap(LSA[0], Spawn, CenterMapX, CenterMapZ, ResetPoint, LSA[4]));
                    }
                } catch (Exception ex) {
                    Bukkit.getLogger().warning(("Problem with map " + i + ex.getLocalizedMessage()));
                }
            }
            if (GamingMaps.isEmpty()) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "No mapz in da GamingMapInfo file!!! RramaGaming plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(RramaGaming.ThisPlugin);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((RramaGaming.RGTagB + "Cannot find da GamingMapInfo file!!! RramaGaming plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(RramaGaming.ThisPlugin);
        }
    }
    
    public static String checkTextureURL(String url) {
        if (url.equals("null")) {
            url = GamingMaps.get("Spawn").getTexture();
        }
        if (url.startsWith("http://www.mediafire.com")) {
            try {
                URL Rurl = new URL(url);
                URLConnection urlC = Rurl.openConnection();
                InputStream urlCis = urlC.getInputStream();
                Scanner s = new Scanner(urlCis);
                while (s.hasNextLine()) {
                    String Line = s.nextLine();
                    if (Line.contains("kNO =")) {
                        urlCis.close();
                        return Line.split("kNO = \"")[1].split("\";")[0];
                    }
                }
                urlCis.close();
                throw new MalformedURLException(); //If reached then the page did not contain that stuff.
            } catch (IOException ex) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Texture pack URL broken '" + url + "'."));
                if (!url.equals(GamingMaps.get("Spawn").getTexture())) {
                    return checkTextureURL("null");
                } else {
                    //Screwed...
                    return "Error";
                }
            }
        } else return url;
    }
    
}
