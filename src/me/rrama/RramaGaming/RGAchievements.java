package me.rrama.RramaGaming;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RGAchievements {
    
    public static boolean PlayerHasAchievement(final String PN, final Achievement A) {
        boolean AB = false;
        File AF = new File(FolderSetup.PlayersAchievementsFolder, System.getProperty("file.separator") + A + ".txt");
        try (Scanner s = new Scanner(AF)) {
            while (s.hasNextLine()) {
                if (s.nextLine().equals(PN)) AB = true;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not find File '" + AF + "'."));
        }
        return AB;
    }
    
    public static void AwardAchievement(final Player[] Ps, final Achievement A) {
        ArrayList<String> PNs = new ArrayList<>();
        for (Player p : Ps) {
            PNs.add(p.getName());
        }
        AwardAchievement(PNs, A);
    }
    
    public static void AwardAchievement(final ArrayList<String> APNs, final Achievement A) {
        ArrayList<String> PNs = new ArrayList<>();
        for (String N : APNs) {
            if (PlayerHasAchievement(N, A) == false) {
                PNs.add(N);
            }
        }
        
        if (PNs.size() > 1) {
            Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.GREEN + ListToString.ListToString(PNs, ", ") + " all earnt the achievement " + A + ".");
            for (String PN : PNs) {
                AwardAchievement(PN, A, false);
            }
        } else if (PNs.size() == 1) {
            for (String PN : PNs) {
                AwardAchievement(PN, A);
            }
        }
    }
    
    public static void AwardAchievement(final String PN, final Achievement A) {
        AwardAchievement(PN, A, true);
    }
    
    public static void AwardAchievement(final String PN, final Achievement A, final boolean B) {
        if (PlayerHasAchievement(PN, A) == false) {
            if (B) {
                Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.GREEN + PN + " has earnt the achievement " + A + ".");
            }
            Player P = Bukkit.getPlayerExact(PN);
            File AF = new File(FolderSetup.PlayersAchievementsFolder, System.getProperty("file.separator") + A + ".txt");
            if (AF.exists() == false) {
                Bukkit.getLogger().warning((RramaGaming.RGTagB + "Could not find file '" + AF + "'. " + PN + " was not added to the list."));
                if (P != null) {
                    P.sendMessage(ChatColor.GOLD + "Something went wrong :( You did not receive the achievement " + A + ". Tell an OP.");
                }
            } else {
                try (FileWriter FWA = new FileWriter(AF, true)) {
                    FWA.write(PN + System.getProperty("line.separator"));
                } catch (IOException ex) {
                    Bukkit.getLogger().warning((RramaGaming.RGTagB + "Failed to write to '" + AF + "'. " + PN + " was not added to the list."));
                    if (P != null) {
                        P.sendMessage(ChatColor.GOLD + "Something went wrong :( You did not receive the achievement " + A + ". Tell an OP.");
                    }
                }
            }
        }
    }
    
    public static enum Achievement {
        
        /**
         * Join the game when no game is in progress, then being at the start of a game.
         */
        Start_At_The_Start,
        
        /**
         * Survive a map reset.
         */
        InB4Kippers,
        
        /**
         * Use the shop.
         */
        Just_Browsing,
        
        /**
         * Earn a cookie.
         */
        Earner,
        
        /**
         * Buy something from the shop.
         */
        Buyer,
        
        /**
         * Care package.
         */
        Gift_From_Heaven,
        
        /**
         * Catch gold when it rains gold.
         */
        Gold_Miner,
        
        /**
         * Vote in a map vote.
         */
        Democracy,
        
        /**
         * Eat some food.
         */
        Eater,
        
        /**
         * Kill the chat (no one talks after you for 60 seconds).
         */
        Chat_Killer,
        
        /**
         * Can you not read the name of it? Well I guess you'll be getting this one.
         */
        Say_Something_Stupid;
    }
}
