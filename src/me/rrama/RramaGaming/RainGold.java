package me.rrama.RramaGaming;

import java.util.ArrayList;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RainGold implements CommandExecutor {
    
    public static ArrayList<String> HasTakenGold = new ArrayList<>();
    public static boolean RainingGold = false;
    
    public static void ScheduleNewGoldRain() {
        final int RTime = RramaGaming.Random.nextInt(24000);
        final int RTime2Use = RTime + 24000;
        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                HasTakenGold.clear();
                RainingGold = true;
                Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.GOLD + "OMG it's raining GOLD!!!1! Quick use /GrabGold to get free cookies!");
                StopDaRainOfGold();
            }

        }, RTime2Use);
    }

    public static void StopDaRainOfGold() {
        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                OpChat.SendMessageToAllOps("RramaGaming", "Gold event is over.");
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (!HasTakenGold.contains(p.getName())) {
                        Bukkit.broadcastMessage(RramaGaming.RGTagNo + ChatColor.GOLD + "All the gold has dried up :( Better luck next time. Use /GrabGold quicker.");
                    }
                }
                RainingGold = false;
                HasTakenGold.clear();
                ScheduleNewGoldRain();
            }

        }, 30*20);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] strings) {
        if (commandLable.equalsIgnoreCase("GrabGold")) {
            if (sender instanceof Player) {
                if (RainingGold) {
                    Player PS = (Player)sender;
                    String PSN = PS.getName();
                    if (HasTakenGold.contains(PSN)) {
                        sender.sendMessage(ChatColor.YELLOW + "You have already taken this batch of gold.");
                    } else {
                        try {
                            HasTakenGold.add(PSN);
                            String PN = PS.getName();
                            Cookies.addCookies(PN, 2);
                            sender.sendMessage(ChatColor.YELLOW + "You sold the gold for 2 cookies.");
                            RGAchievements.AwardAchievement(PN, Achievement.Gold_Miner);
                        } catch (CookieFileException ex) {
                            ex.sendConsoleMessage();
                            ex.sendPlayerMessage();
                        }
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "It is not currently raining any gold.");
                }
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You must be a player to use this command.");
            }
            return true;
        } else return false;
    }
}