package me.rrama.RramaGaming;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Chat implements Listener {
    
    public static HashMap<String, String> CencoredList = new HashMap<>();
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        Player P = event.getPlayer();
        String PN = P.getName();
        Gamer G = RramaGaming.getGamer(PN);
        if (G.getPreAppendededMessage() != null) {
            event.setMessage(G.getPreAppendededMessage() + event.getMessage());
            G.setPreAppendededMessage(null);
        }
        if (event.getMessage().endsWith("/a")) {
            event.setMessage(event.getMessage().replaceAll("/a", " "));
            G.setPreAppendededMessage(event.getMessage());
            P.sendMessage(ChatColor.GOLD + "Message Appended.");
            event.setCancelled(true);
            return;
        }
        for (String s : CencoredList.keySet()) {
            String s2 = CencoredList.get(s);
            event.setMessage(event.getMessage().replaceAll(s, s2));
        }
        if (event.getMessage().startsWith("#")) {
            event.setMessage(event.getMessage().replaceFirst("#", ""));
            OpChat.SendMessageToAllOps(PN, event.getMessage());
        } else if (event.getMessage().startsWith("@")) {
            event.setMessage(event.getMessage().replaceFirst("@", ""));
            Bukkit.dispatchCommand(P, "msg " + event.getMessage());
        } else {
            ChatColor PNC = G.getNameColour();
            if (G.getTitle() != null) {
                String TW = ("[" + G.getTitle() + "] ");
                ChatColor TC = G.getTitleColour();
                String ConsoleMessage = TC + TW + PNC + PN + ChatColor.RESET + ": " + event.getMessage();
                CommandAsConsole.IgnoreFromSending.add(ConsoleMessage);
                Bukkit.getLogger().info(ConsoleMessage);
                for (Player Rer : event.getRecipients()) {
                    final String PMessage2Send = SortPersonalMessages(event.getMessage(), Rer.getName());
                    Rer.sendMessage(TC + TW + PNC + PN + ChatColor.RESET + ": " + PMessage2Send);
                }
            } else {
                Bukkit.getLogger().info((PNC + PN + ChatColor.RESET + ": " + event.getMessage()));
                for (Player Rer : event.getRecipients()) {
                    final String PMessage2Send = SortPersonalMessages(event.getMessage(), Rer.getName());
                    Rer.sendMessage(PNC + PN + ChatColor.RESET + ": " + PMessage2Send);
                }
            }
        }
    }
    
    public static String SortPersonalMessages(final String Message, final String PN) {
        String PMessage2Send = Message;
        if (PMessage2Send.contains(PN)) {
            if (PMessage2Send.equals(PN)) {
                PMessage2Send = ChatColor.BOLD + PN;
            } else {
                StringBuilder SB = new StringBuilder();
                String[] Split = PMessage2Send.split(PN);
                int last = Split.length;
                if (PMessage2Send.endsWith(PN)) last++;
                int i = 1;
                for (String S : Split) {
                    if (i < last) {
                        String Colour = ChatColor.getLastColors(S);
                        ChatColor LC;
                        if (Colour.equals("")) {
                            LC = ChatColor.WHITE;
                        } else {
                            LC = ChatColor.valueOf(Colour);
                        }
                        SB.append((S + ChatColor.BOLD + PN + ChatColor.RESET + LC));
                        i++;
                    } else {
                        SB.append(S);
                    }
                }
                PMessage2Send = SB.toString();
            }
        }
        if (PMessage2Send.contains("$name")) {
            if (PMessage2Send.equals("$name")) {
                PMessage2Send = ChatColor.BOLD + "$" + PN;
            } else {
                StringBuilder SB = new StringBuilder();
                String[] Split = PMessage2Send.split("\\$name");
                int last = Split.length;
                if (PMessage2Send.endsWith("$name")) last++;
                int i = 1;
                for (String S : Split) {
                    if (i < last) {
                        String Colour = ChatColor.getLastColors(S);
                        ChatColor LC;
                        if (Colour.equals("")) {
                            LC = ChatColor.WHITE;
                        } else {
                            LC = ChatColor.valueOf(Colour);
                        }
                        SB.append((S + ChatColor.BOLD + "$" + PN + ChatColor.RESET + LC));
                        i++;
                    } else {
                        SB.append(S);
                    }
                }
                PMessage2Send = SB.toString();
            }
        }
        return PMessage2Send;
    }
}