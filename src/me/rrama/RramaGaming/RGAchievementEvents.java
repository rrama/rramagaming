package me.rrama.RramaGaming;

import java.util.ArrayList;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

public class RGAchievementEvents implements Listener {
    
    public static BukkitTask LastChatterTime;
    public static String LastChatter = "";
    public static ArrayList<String> JoinedB4Start = new ArrayList<>();
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        String PN = event.getPlayer().getName();
        if ("null".equals(RramaGaming.gameInPlay)) {
            if (!(JoinedB4Start.contains(PN) || RGAchievements.PlayerHasAchievement(PN, Achievement.Start_At_The_Start))) {
                JoinedB4Start.add(PN);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        
        String PN = event.getPlayer().getName();
        if (JoinedB4Start.contains(PN)) {
            JoinedB4Start.remove(PN);
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) return;
        
        final String PN = event.getPlayer().getName();
        if (RramaGaming.getGamer(PN).getRank() == Rank.Guest) {
            RGAchievements.AwardAchievement(PN, Achievement.Say_Something_Stupid);
        }
        
        if (LastChatter.equals(PN)) return;
        if (LastChatterTime != null) {
            LastChatterTime.cancel();
        }
        LastChatterTime = Bukkit.getScheduler().runTaskLaterAsynchronously(RramaGaming.ThisPlugin, new Runnable() {

            @Override
            public void run() {
                RGAchievements.AwardAchievement(PN, Achievement.Chat_Killer);
            }
            
        }, 1200);
    }
}