package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OpChat implements CommandExecutor {
    
    public static String OpChatTag = ChatColor.AQUA + "[Op Chat] ";
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("OpChat") || commandLable.equalsIgnoreCase("OC") || commandLable.equalsIgnoreCase("O")) {
            if (args.length > 0) {
                String SenderName = sender.getName();
                String CMDMessage = ListToString.ListToString("", args, " ", 0, 0, "");
                SendMessageToAllOps(SenderName, CMDMessage);
            } else {
                sender.sendMessage(ChatColor.YELLOW + "You have not entered a message.");
            }
            return true;
        } else return false;
    }
    
    public static void SendMessageToAllOps(final String Name, final String Message) {
        String msg = OpChatTag + Name + ": " + Message;
        CommandAsConsole.IgnoreFromSending.add(msg);
        Bukkit.getLogger().info(msg);
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.isOp()) {
                p.sendMessage(msg);
            }
        }
    }
}