package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.CarePackage;
import me.rrama.RramaGaming.GamingPlugin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ShopCarePackage extends ShopItem {
    
    public ShopCarePackage(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (me.rrama.RramaGaming.CarePackage.CarePackagers.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You already have a Care Package to use.");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        CarePackage.CarePackagers.add(P.getName());
        P.sendMessage(ChatColor.YELLOW + "Click a block to call in the Care Package.");
        return true;
    }
    
}