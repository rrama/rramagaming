package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.entity.Player;

public class ListToString {
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param AS String[] - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     */
    public static String ListToString(String[] AS, String Seperator) {
        return ListToString("", AS, Seperator, 0, 0, "");
    }
    /**
     * Returns a string constructed out of the names of players in the specified list, separated by
     * the specified separator.
     * @return A string constructed out of the player list given.
     * @param list - a list of players that you want to turn into a single string.
     * @param separator - the string to separate out the elements in the player list
     */
    public static String playerListToString(Player[] players, String separator) {
    	ArrayList<String> names = new ArrayList<>();
    	for (Player P : players) {
            names.add(P.getName());
        }
    	return ListToString("", names, separator, 0, 0, "");	
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param AS ArrayList - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     */
    public static String ListToString(ArrayList<String> AS, String Seperator) {
        return ListToString("", AS, Seperator, 0, 0, "");
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param Starter String - at the start of the String returned.
     * @param AS String[] - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param Ender String - at the end of the String returned.
     */
    public static String ListToString(String Starter, String[] AS, String Seperator, String Ender) {
        return ListToString(Starter, AS, Seperator, 0, 0, Ender);
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param Starter String - at the start of the String returned.
     * @param AS ArrayList - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param Ender String - at the end of the String returned.
     */
    public static String ListToString(String Starter, ArrayList<String> AS, String Seperator, String Ender) {
        return ListToString(Starter, AS, Seperator, 0, 0, Ender);
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param AS String[] - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param IgnoreFirst Integer - the number of values in AS you want to skip at the start.
     * @param IgnoreLast Integer - the number of values in AS you want to skip at the end.
     */
    public static String ListToString(String[] AS, String Seperator, int IgnoreFirst, int IgnoreLast) {
        return ListToString("", AS, Seperator, IgnoreFirst, IgnoreLast, "");
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param AS ArrayList - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param IgnoreFirst Integer - the number of values in AS you want to skip at the start.
     * @param IgnoreLast Integer - the number of values in AS you want to skip at the end.
     */
    public static String ListToString(ArrayList<String> AS, String Seperator, int IgnoreFirst, int IgnoreLast) {
        return ListToString("", AS, Seperator, IgnoreFirst, IgnoreLast, "");
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param Starter String - at the start of the String returned.
     * @param AS String[] - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param IgnoreFirst Integer - the number of values in AS you want to skip at the start.
     * @param IgnoreLast Integer - the number of values in AS you want to skip at the end.
     * @param Ender String - at the end of the String returned.
     */
    public static String ListToString(String Starter, String[] AS, String Seperator, int IgnoreFirst, int IgnoreLast, String Ender) {
        ArrayList<String> AS2 = new ArrayList<>();
        AS2.addAll(Arrays.asList(AS));
        return ListToString(Starter, AS2, Seperator, IgnoreFirst, IgnoreLast, Ender);
    }
    
    /**
     * 
     * @return A string constructed out of the list of Gamers given.
     * 
     * @param Starter String - at the start of the String returned.
     * @param AS ArrayList - of gamers that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param IgnoreFirst Integer - the number of values in AS you want to skip at the start.
     * @param IgnoreLast Integer - the number of values in AS you want to skip at the end.
     * @param Ender String - at the end of the String returned.
     */
    public static String GamerListToString(String Starter, ArrayList<Gamer> AG, String Seperator, String Ender) {
        ArrayList<String> ASN = new ArrayList<>();
        for (Gamer G : AG) {
            ASN.add(G.getName());
        }
        return ListToString(Starter, ASN, Seperator, Ender);
    }
    
    /**
     * 
     * @return A string constructed out of the objects given.
     * 
     * @param Starter String - at the start of the String returned.
     * @param AS ArrayList - that you want to turn into a single String.
     * @param Seperator String - to separate out the elements in AS.
     * @param IgnoreFirst Integer - the number of values in AS you want to skip at the start.
     * @param IgnoreLast Integer - the number of values in AS you want to skip at the end.
     * @param Ender String - at the end of the String returned.
     */
    public static String ListToString(String Starter, ArrayList<String> AS, String Seperator, int IgnoreFirst, int IgnoreLast, String Ender) { //All Constructors eventually point to this... Eventually.
        ArrayList<String> AS2 = new ArrayList<>();
        int i = 1;
        int i2 = AS.size();
        //For loop could be used so you don't have to use the i int
        //And they're faster than foreach loops, says barry
        for (String s : AS) { 
            if (i > IgnoreFirst && i2 > IgnoreLast) {
                AS2.add(s);
            }
            i++;
            i2 -= 1;
        }
        
        String Done = Starter;
        int iy = 1;
        for (String s : AS2) { //could be used with a for loop, so you don't have to add this iy int.
            if (iy == 1) {
                Done = Done + s;
            } else {
                Done = Done + Seperator + s;
            }
            iy++;
        }
        Done += Ender;
        return Done;
    }
}
