package me.rrama.RramaGaming;

import java.util.ArrayList;
import me.rrama.RramaGaming.RGAchievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RGAchievementCommands implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Achievements") || commandLable.equalsIgnoreCase("Awards")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    final String PN = ((Player)sender).getName();
                    ArrayList<String> Havers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        if (RGAchievements.PlayerHasAchievement(PN, A)) {
                            Havers.add(A.name());
                        }
                    }
                    if (Havers.isEmpty()) {
                        sender.sendMessage(ChatColor.YELLOW + "You have no core achievements.");
                    } else {
                        final String All = ListToString.ListToString(Havers, ", ");
                        sender.sendMessage(ChatColor.YELLOW + "You have the core achievements: " + All + ".");
                    }
                    for (String GameN : RramaGaming.EnabledGames) {
                        Bukkit.dispatchCommand(sender, GameN + "Achievements");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Consoles do not get achievements, other than being the best :)");
                }
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("All")) {
                    ArrayList<String> Allers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        Allers.add(A.name());
                    }
                    final String All = ListToString.ListToString(Allers, ", ");
                    sender.sendMessage(ChatColor.YELLOW + "Avalible core achievements: " + All + ".");
                    for (String GameN : RramaGaming.EnabledGames) {
                        Bukkit.dispatchCommand(sender, GameN + "Achievements All");
                    }
                } else {
                    Player P = Bukkit.getPlayer(args[0]);
                    if (P == null) {
                        sender.sendMessage(ChatColor.YELLOW + "Could not find the player spesicied.");
                    } else {
                        final String PN = P.getName();
                        ArrayList<String> Havers = new ArrayList<>();
                        for (Achievement A : Achievement.values()) {
                            if (RGAchievements.PlayerHasAchievement(PN, A)) {
                                Havers.add(A.name());
                            }
                        }
                        if (Havers.isEmpty()) {
                            sender.sendMessage(ChatColor.YELLOW + PN + " has no core achievements.");
                        } else {
                            final String All = ListToString.ListToString(Havers, ", ");
                            sender.sendMessage(ChatColor.YELLOW + PN + " has the core achievements: " + All + ".");
                        }
                        for (String GameN : RramaGaming.EnabledGames) {
                        Bukkit.dispatchCommand(sender, GameN + "Achievements " + PN);
                    }
                    }
                }
                return true;
            } else return false;
        } else return false;
    }
}
