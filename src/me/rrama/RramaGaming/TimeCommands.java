package me.rrama.RramaGaming;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TimeCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Time") || commandLable.equalsIgnoreCase("T")) {
            if (RramaGaming.gameInPlay.equals("null")) {
                sender.sendMessage(ChatColor.YELLOW + "No game in play.");
                return true;
            }
            GamingPlugin GP = (GamingPlugin) Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
            if (!GP.RoundPlugin) {
                sender.sendMessage(ChatColor.YELLOW + GP.getName() + " is not a rounded plugin.");
                return true;
            }

            if (RramaGaming.R == RoundState.PreGame) {
                sender.sendMessage(ChatColor.YELLOW + "Round starts in: " + ChatColor.RED + GP.timer.getTime() + " seconds.");
            } else if (RramaGaming.R == RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "Round auto ends in: " + ChatColor.RED + GP.timer.getTime() + " seconds.");
            } else if (RramaGaming.R == RoundState.Voting) {
                sender.sendMessage(ChatColor.YELLOW + "Map vote is in progress.");
            } else if (RramaGaming.R == RoundState.MapReset) {
                sender.sendMessage(ChatColor.YELLOW + "The next map is reseting, please wait.");
            } else if (RramaGaming.R == RoundState.AfterGame) {
                sender.sendMessage(ChatColor.YELLOW + "The round and posibly " + GP.getName() + " is ending.");
            } else if (RramaGaming.R == RoundState.NeedMorePlayers) {
                sender.sendMessage(ChatColor.YELLOW + "Need more players before the round can begin.");
            } else if (RramaGaming.R == RoundState.Off) {
                sender.sendMessage(ChatColor.YELLOW + GP.getName() + " is in play, but round is off... BREAK BREAK!");
            } else {
                sender.sendMessage(ChatColor.YELLOW + GP.getName() + "is in play, but in an unexpected round state '" + RramaGaming.R.name() + "'.");
            }
            return true;
            
        } else if (commandLable.equalsIgnoreCase("AddTime")) {
            if (args.length == 0) return false;
            if (RramaGaming.gameInPlay.equals("null")) {
                sender.sendMessage(ChatColor.YELLOW + "No game in play.");
                return true;
            }
            GamingPlugin GP = (GamingPlugin) Bukkit.getPluginManager().getPlugin(RramaGaming.gameInPlay);
            if (!GP.RoundPlugin) {
                sender.sendMessage(ChatColor.YELLOW + GP.getName() + " is not a rounded plugin.");
                return true;
            }
            if (!GP.AllowAddingTime) {
                sender.sendMessage(ChatColor.YELLOW + GP.getName() + " does not allow adding to the time.");
                return true;
            }
            if (RramaGaming.R != RoundState.PreGame && RramaGaming.R != RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "Can not add time during this current round state.");
                return true;
            }
            if (GP.timer.getTime() < 10) {
                sender.sendMessage(ChatColor.YELLOW + "Too late to add time. (< 10 seconds left)");
                return true;
            }
            try {
                GP.timer.addTime(Integer.parseInt(args[0]));
                OpChat.SendMessageToAllOps(GP.getName(), sender.getName() + " added " + args[0] + " seconds to the round time.");
            } catch (NumberFormatException ex) {
                sender.sendMessage(ChatColor.YELLOW + "That is not a valid time");
                return false;
            }
            return true;
            
        } else return false;
    }

}