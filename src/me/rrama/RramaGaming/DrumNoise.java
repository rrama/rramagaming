package me.rrama.RramaGaming;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Note;
import org.bukkit.Note.Tone;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DrumNoise implements CommandExecutor { //Stuff happens here... :( Works DW.
    
    public static boolean in = false; //is drumnoise being played?
    public static HashMap<Player, Location> PlayerLocation = new HashMap<>(); //guess
    
    public static HashMap<Player, Location> la = new HashMap<>(); //block a location
    public static HashMap<Player, Integer> laB = new HashMap<>(); //block a type
    public static HashMap<Player, Byte> laD = new HashMap<>(); //block a data
    
    public static HashMap<Player, Location> lb = new HashMap<>(); //block b location
    public static HashMap<Player, Integer> lbB = new HashMap<>(); //block b type
    public static HashMap<Player, Byte> lbD = new HashMap<>(); //block b data
    
    public static HashMap<Player, Location> lc = new HashMap<>(); //block c location
    public static HashMap<Player, Integer> lcB = new HashMap<>(); //block c type
    public static HashMap<Player, Byte> lcD = new HashMap<>(); //block c data
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("DrumNoise")) {
            if (in) {
                sender.sendMessage(ChatColor.YELLOW + "Drum noise is already being played right not. Please wait.");
            } else {
                in = true;
                Bukkit.broadcastMessage(ChatColor.RED + "BA " + ChatColor.YELLOW + "DUM " +
                        ChatColor.GOLD + "T" + ChatColor.AQUA + "s" + ChatColor.BLACK + "s" +
                        ChatColor.BLUE + "h" + ChatColor.WHITE + "h" + ChatColor.GREEN + "h");
                NoteBlocksNoise();
            }
            return true;
        } else return false;
    }
    
    public static void NoteBlocksNoise() {
        
        la.clear();
        laB.clear();
        laD.clear(); //cleaning
        
        for (Player p : Bukkit.getOnlinePlayers()) { //send player new blocks to be played on.
            PlayerLocation.put(p, p.getLocation().getBlock().getLocation().clone().add(0.0, -1.0, 0.0));
            //next block readying V
            la.put(p, PlayerLocation.get(p).clone().add(1.0, 0.0, 0.0)); //guess.
            laB.put(p, la.get(p).getBlock().getTypeId()); //store block a type
            laD.put(p, la.get(p).getBlock().getData()); //store block a data
            
            p.sendBlockChange(la.get(p), 25, Byte.parseByte("0"));
        }
        
        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() { //call layer 1

            @Override
            public void run() {
                //inside layer 1
                lb.clear();
                lbB.clear();
                lbD.clear(); //cleaning
                
                for (Player p : Bukkit.getOnlinePlayers()) { //play note a for each player
                    p.playNote(la.get(p), Instrument.BASS_DRUM, Note.natural(0, Tone.B));
                    //next block readying V
                    lb.put(p, PlayerLocation.get(p).clone().add(-1.0, 0.0, 0.0));//guess
                    lbB.put(p, lb.get(p).getBlock().getTypeId()); //store block a type
                    lbD.put(p, lb.get(p).getBlock().getData()); //store block a data
                    
                    p.sendBlockChange(lb.get(p), 25, Byte.parseByte("0"));
                }
                
                
                Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() { //call layer 2

                    @Override
                    public void run() {
                        //inside layer 2
                        lc.clear();
                        lcB.clear();
                        lcD.clear(); //cleaning
                        
                        for (Player p : Bukkit.getOnlinePlayers()) { //play note b for each player
                            p.sendBlockChange(la.get(p), laB.get(p), laD.get(p)); //restore block a
                            
                            p.playNote(lb.get(p), Instrument.BASS_DRUM, Note.sharp(1, Tone.D));
                            //next block readying V
                            lc.put(p, PlayerLocation.get(p).clone().add(0.0, 0.0, 1.0));//guess
                            lcB.put(p, lb.get(p).getBlock().getTypeId()); //store block a type
                            lcD.put(p, lb.get(p).getBlock().getData()); //store block a data

                            p.sendBlockChange(lc.get(p), 25, Byte.parseByte("0"));;
                        }
                        la.clear();
                        laB.clear();
                        laD.clear(); //Don't need no more.
                        
                        Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() { //call layer 3

                            @Override
                            public void run() {
                                //inside layer 3
                                for (Player p : Bukkit.getOnlinePlayers()) { //play note c for each player
                                    p.sendBlockChange(lb.get(p), lbB.get(p), lbD.get(p)); //restore block b
                                    
                                    p.playNote(lc.get(p), Instrument.SNARE_DRUM, Note.sharp(0, Tone.F));
                                }
                                lb.clear();
                                lbB.clear();
                                lbD.clear();
                                
                                Bukkit.getScheduler().runTaskLater(RramaGaming.ThisPlugin, new Runnable() { //call layer 4

                                    @Override
                                    public void run() {
                                        //inside layer 4
                                        for (Player p : Bukkit.getOnlinePlayers()) { //clean up.
                                            p.sendBlockChange(lc.get(p), lcB.get(p), lcD.get(p)); //restore block c
                                        }
                                        lc.clear();
                                        lcB.clear();
                                        lcD.clear();
                                        
                                        PlayerLocation.clear();
                                        
                                        in = false;
                                        //end layer 4
                                    }

                                }, 5);
                                //end layer 3
                            }

                        }, 5);
                        //end layer 2
                    }

                }, 5);
                //end layer 1
            }
            
        }, 5);
    }
}