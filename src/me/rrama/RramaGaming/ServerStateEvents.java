package me.rrama.RramaGaming;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class ServerStateEvents implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void PlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getPlayer().hasPermission("bukkit.command.reload")) {
            checkReload(event.getMessage());
        }
    }
    @EventHandler(priority = EventPriority.NORMAL)
    public void ServerCommand(ServerCommandEvent event) {
        checkReload(event.getCommand());
    }
    
    private void checkReload(String cmd) {
        if (cmd.equalsIgnoreCase("/Reload") || cmd.equalsIgnoreCase("Reload")) {
            ServerState.setServerState(ServerState.Reload);
        } else if (cmd.equalsIgnoreCase("/Stop") || cmd.equalsIgnoreCase("Stop")) {
            ServerState.setServerState(ServerState.Stopping);
        }
    }
    
}