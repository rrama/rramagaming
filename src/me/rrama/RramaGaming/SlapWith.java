package me.rrama.RramaGaming;

import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SlapWith implements CommandExecutor {

    public enum ThingsToSlapWith { // If the player does not chose their own.
            Brick, Plank, Wolf, rrama, Cheese, Enderpearl, Ocelot, Pikachu, Parrot, Sponge, Baillie,
    }

    public static void RandomSlapDemUp(final String SN, final String VN) {
            Random Random = new Random();
            ThingsToSlapWith ThingToSlapWith = ThingsToSlapWith.values()[Random.nextInt(ThingsToSlapWith.values().length)];
            String ThingToSlapWithN = ThingToSlapWith.name();
            SlapDemUp(SN, VN, ThingToSlapWithN);
    }

    public static void SlapDemUp(final String SN, final String VN, final String S) {
            String AnA;
            if (S.startsWith("a") || S.startsWith("e") || S.startsWith("i")
                            || S.startsWith("o") || S.startsWith("u") || S.startsWith("A")
                            || S.startsWith("E") || S.startsWith("I") || S.startsWith("O")
                            || S.startsWith("U")) {
                AnA = "an";
                
            } else {
                AnA = "a";
            }
            Bukkit.broadcastMessage(ChatColor.GREEN + SN + " slapped " + VN
                            + " with " + AnA + " " + ChatColor.WHITE + S + ChatColor.GREEN
                            + ".");
            if (SN.equalsIgnoreCase("Xdoncorleone") && VN.equalsIgnoreCase("llamasaylol")) {
                RandomSlapDemUp(VN, SN); //retaliate :P
            }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("SlapWith") && (args.length == 1 || args.length == 2)) {
                Player Victim = Bukkit.getPlayer(args[0]);
                if (Victim != null) {
                    String SN = sender.getName();
                    String VN = Victim.getName();
                    if (args.length == 1) {
                        RandomSlapDemUp(SN, VN);
                    } else {
                        SlapDemUp(SN, VN, args[1]);
                    }
                    return true;
                } else return false;
            } else return false;
    }
}