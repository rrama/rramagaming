package me.rrama.RramaGaming;

import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class Votifier implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onVotifier(VotifierEvent event) {
        com.vexsoftware.votifier.model.Vote V = event.getVote();
        OfflinePlayer OP = Bukkit.getOfflinePlayer(V.getUsername());
        OfflineGamer OG = RramaGaming.getGamer(V.getUsername());
        try {
            OG.addCookies(5);
            Bukkit.broadcastMessage(RramaGaming.RGTagB + OP.getName() + " voted for Degnebolig's on " + V.getServiceName() + " and got 5 free cookies!");
        } catch (CookieFileException ex) {
            if (OP.isOnline()) {
                Player P = Bukkit.getPlayer(V.getUsername());
                P.sendMessage(ChatColor.RED + "We were unable to add cookies for your vote. We shall try again.");
                Gamer G = RramaGaming.getGamer(V.getUsername());
                try {
                    G.addCookies(5);
                } catch (CookieFileException ex1) {
                    P.sendMessage(ChatColor.GOLD + "Our second attempt to give you cookies failed. Report this to an Mod+. Please test that /cookies still works.");
                }
            }
        }
    }

}