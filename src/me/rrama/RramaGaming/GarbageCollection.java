package me.rrama.RramaGaming;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class GarbageCollection implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("GarbageCollection") || commandLable.equalsIgnoreCase("GC")) {
            OpChat.SendMessageToAllOps("RramaGaming", sender.getName() + " called a garbage collection.");
            System.gc();
            sender.sendMessage(ChatColor.YELLOW + "You sucessfully called a garbage collection.");
            return true;
        } else return false;
    }
    
}