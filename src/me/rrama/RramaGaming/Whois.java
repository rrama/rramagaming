package me.rrama.RramaGaming;

import java.net.InetSocketAddress;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Whois implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Whois") && args.length == 1) {
            Player p = Bukkit.getPlayer(args[0]);
            if (p != null) {
                String DisplayName = p.getDisplayName();
                Location Location = p.getLocation();
                String World = Location.getWorld().getName();
                boolean IsOp = p.isOp();
                GameMode GameMode = p.getGameMode();
                InetSocketAddress Address = p.getAddress();
                int Health = p.getHealth();
                int FoodBar = p.getFoodLevel();
                Rank rank = RramaGaming.getGamer(p.getName()).getRank();

                sender.sendMessage(ChatColor.AQUA + "  -- " + DisplayName + " --  ");
                sender.sendMessage(ChatColor.AQUA + "World: " + World);
                sender.sendMessage(ChatColor.AQUA + "Coords: " + Location.getBlockX() + " " + Location.getBlockY() + " " + Location.getBlockZ());
                sender.sendMessage(ChatColor.AQUA + "Health: " + Health + "/20");
                sender.sendMessage(ChatColor.AQUA + "FoodBar: " + FoodBar + "/20");
                sender.sendMessage(ChatColor.AQUA + "GameMode: " + GameMode);
                sender.sendMessage(ChatColor.AQUA + "IP Address: " + Address);
                sender.sendMessage(ChatColor.AQUA + "Rank: " + rank.name());
                sender.sendMessage(ChatColor.AQUA + "OP: " + IsOp);
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Could not find the player you were looking for. Trying WhoWas.");
                Bukkit.dispatchCommand(sender, "WhoWas " + args[0]);
            }
            return true;
        } else if (commandLable.equalsIgnoreCase("WhoWas") && args.length == 1) {
            OfflinePlayer OP = Bukkit.getOfflinePlayer(args[0]);
            if (OP.isOnline()) {
                sender.sendMessage(ChatColor.YELLOW + OP.getName() + " is online. Trying Whois.");
                Bukkit.dispatchCommand(sender, "WhoIs " + OP.getName());
            } else if (OP.hasPlayedBefore()) {
                sender.sendMessage(ChatColor.AQUA + "  -- " + OP.getName() + " --  ");
                sender.sendMessage(ChatColor.AQUA + "Last Online: " + OP.getLastPlayed());
                sender.sendMessage(ChatColor.AQUA + "OP: " + OP.isOp());
                sender.sendMessage(ChatColor.AQUA + "Banned: " + OP.isBanned());
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Could not find the player you were looking for.");
            }
            return true;
        } else return false;
    }
}
