package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.Shop;
import org.bukkit.entity.Player;

public class ShopItem {
    
    final String name;
    final int price;

    public ShopItem(final GamingPlugin GP, final String Name, final int Price) {
        name = Name;
        price = Price;
        if (GP == null) {
            Shop.GamingShopItems.add(this);
        } else {
            GP.ShopItems.add(this);
        }
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public boolean isVisibleTo(final Player P) {
        return true;
    }
    
    public boolean canBuy(final Player P) {
        return true;
    }
    
    public boolean perform(final Player P) {
        return false;
    }
    
}