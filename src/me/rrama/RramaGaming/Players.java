package me.rrama.RramaGaming;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Players implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Players")) {
            HashMap<Rank, ArrayList<String>> Ranked = new HashMap<>();
            ArrayList<String> BannedN = new ArrayList<>();
            ArrayList<String> UnrankedN = new ArrayList<>();
            
            for (Rank R : Rank.values()) {
                Ranked.put(R, new ArrayList<String>());
            }
            Ranked.remove(Rank.Unranked);
            
            for (Gamer G : RramaGaming.getGamers()) {
                final Rank R = G.getRank();
                final String PN = G.getName();
                if (G.getPlayer().isBanned()) {
                    BannedN.add(PN);
                } else if (R.equals(Rank.Unranked)) {
                    UnrankedN.add(PN);
                } else {
                    Ranked.get(R).add(PN);
                }
            }
            
            if (Bukkit.getOnlinePlayers().length == 1) {
                sender.sendMessage(ChatColor.YELLOW + "There is currently 1 player online.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "There are currently " + Bukkit.getOnlinePlayers().length + " players online.");
            }
            for (Rank R : Ranked.keySet()) {
                sender.sendMessage(R.Colour() + R.name() + "s: " + ListToString.ListToString(Ranked.get(R), ", "));
            }
            if (!BannedN.isEmpty()) {
                sender.sendMessage(Rank.Banned.Colour() + "Banned: " + ListToString.ListToString(BannedN, ", "));
            }
            if (!UnrankedN.isEmpty()) {
                sender.sendMessage(ChatColor.UNDERLINE + "Unranked: " + ListToString.ListToString(UnrankedN, ", "));
            }
            return true;
        } else return false;
    }
}
