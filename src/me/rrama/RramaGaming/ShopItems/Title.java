package me.rrama.RramaGaming.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.TitleCMDs;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Title extends ShopItem {
    
    public Title(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (TitleCMDs.HasATitleToUse.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You already have a title to use. Use /Title [Title].");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        TitleCMDs.HasATitleToUse.add(P.getName());
        return true;
    }
    
}